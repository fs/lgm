from __future__ import print_function
from argparse import ArgumentParser
import os, sys
from urlparse import urlparse


p = ArgumentParser()
p.add_argument("--sources", default="video_sources.txt")
p.add_argument("--outpath", default="videos")
p.add_argument("--limit", type=int, default=None)
p.add_argument("--verbose", default=False, action="store_true")
p.add_argument("--dry-run", default=False, action="store_true")
args = p.parse_args()

sources = []
with open(args.sources) as f:
	for line in f:
		line = line.rstrip()
		if line:
			src = line.split()[0]
			sources.append(src)

for src in sources:
	year = src.split("/")[0]
	youtube = "youtube" in src
	print ("downloading {0} videos".format(year), file=sys.stderr)
	with open(src) as f:
		count = 0
		for line in f:
			url = line.rstrip()
			if url:
				outpath = os.path.join(args.outpath, year)
				try:
					os.makedirs(outpath)
				except OSError:
					pass

				if youtube:
					cmd = 'youtube-dl --no-playlist --prefer-free-formats "{0}" --output "{1}/%(title)s-%(id)s.%(ext)s"'.format(url, outpath)
					if args.verbose:
						print (cmd, file=sys.stderr)
					if not args.dry_run:
						os.system(cmd)
				else:
					p = urlparse(url)
					_, filename = os.path.split(p.path)
					filepath = os.path.join(outpath, filename)
					cmd = 'wget "{0}" -O "{1}"'.format(url, filepath)
					if args.verbose:
						print (cmd, file=sys.stderr)
					if os.path.exists(filepath) and os.path.getsize(filepath) > 0:
						print ("File already exists, skipping", file=sys.stderr)
					else:
						if not args.dry_run:
							os.system(cmd)
				count += 1
				if args.limit and count >= args.limit:
					break
		print ("Downloaded {0} videos.".format(count), file=sys.stderr)
