from subprocess import check_output
from random import shuffle, randint
from time import sleep
import os, sys
from argparse import ArgumentParser


p = ArgumentParser()
p.add_argument("--length", type=int, default=15)
p.add_argument("--max-start", type=int, default=60*15)
p.add_argument("--min-start", type=int, default=0)
p.add_argument("--list", action="store_true", default=False)
p.add_argument("--verbose", action="store_true", default=False)
p.add_argument("--mplayer", action="store_true", default=False, help="use mplayer instead of cvlc")
args = p.parse_args()

cmd = 'find videos -iname "*.ogg" -or -iname "*.ogv" -or -iname "*.webm" -or -iname "*.flv" -or -iname "*.mp4"'
vids = check_output(cmd, shell=True).strip().splitlines()

if args.list:
	for v in vids:
		print v
	sys.exit(0)

while True:
	shuffle(vids)

	for item in vids:
		randstart = randint(args.min_start, args.max_start)
		if args.mplayer:
			if args.verbose:
				cmd = 'mplayer -fs "{0}" -ss {1} -endpos {2}'
			else:
				cmd = 'mplayer -fs "{0}" -ss {1} -endpos {2} > /dev/null 2> /dev/null'
		else:
			if args.verbose:
				cmd = 'cvlc --fullscreen --start-time={1} --run-time={2} --play-and-exit "{0}"'
			else:
				cmd = 'cvlc --fullscreen --start-time={1} --run-time={2} --play-and-exit "{0}" > /dev/null 2> /dev/null'
		cmd = cmd.format(item, randstart, args.length)
		print cmd
		os.system(cmd)
		sleep(0.5)
