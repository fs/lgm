from argparse import ArgumentParser
p = ArgumentParser()
p.add_argument("--sources", default="video_sources.txt")
args = p.parse_args()




print """<!DOCTYPE>
<meta charset="utf-8" />
<body contenteditable>
	<table>
		<thead>
		<tr>
			<td>URL</td>
		</tr>
		</thead>
		<tbody>
"""
with open(args.sources) as f:
	for path in f:
		path = path.rstrip()
		if path:
			path = path.split()[0]
			with open(path) as f2:
				for url in f2:
					url = url.strip()
					if url:
						print """<tr><td>{0}</td></tr>""".format(url)

print """		</tbody>
	</table>
</body>
"""
