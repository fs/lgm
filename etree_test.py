from cssselect2 import ElementWrapper
from urllib2 import urlopen
import html5lib, re, sys, json
from xml.etree import ElementTree as ET 

def text (e):
	ret = u''
	if e.text:
		ret += e.text
	ret += u''.join([text(x) for x in e])
	return ret

def linkify(text):
	text = text.replace(" ", "_")
	return "/i/"+text+".html"

with open("2014/program.html") as f:
	t = html5lib.parse(f.read(), namespaceHTMLElements = False)
	doc = ElementWrapper.from_html_root(t)
	for r in doc.query_all(".schedule_item"):
		# print r.etree_element
		for p in r.query_all(".schedule_presenter"):
			# print "  ", p.etree_element
			# for index, elt in enumerate(p.etree_element):
			ptext = text(p.etree_element).strip()
			if ptext:
				for c in p.etree_element:
					p.etree_element.remove(c)
				p.etree_element.text = ''
				a = ET.SubElement(p.etree_element, "a")
				a.set("href", linkify(ptext))
				a.set("rel", "speaker")
				a.text = ptext

print ET.tostring(t, method="html")
