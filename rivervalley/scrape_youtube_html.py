from cssselect2 import ElementWrapper
from urllib2 import urlopen
import html5lib, json, sys
from xml.etree import ElementTree as ET 
from urlparse import urljoin


# <a class="yt-uix-sessionlink yt-uix-tile-link  spf-link  yt-ui-ellipsis yt-ui-ellipsis-2" dir="ltr" title="Laidout and Desktop Publishing" aria-describedby="description-id-881799" data-sessionlink="feature=c4-search&amp;ei=BKkmVYq2DIakqgXy7IK4Aw&amp;ved=CIgBEL8b" href="/watch?v=zLKjdUGu02A">Laidout and Desktop Publishing</a>
base_url = "https://www.youtube.com/"

seen = {}

# open("river_valley_scrape.html")
t = html5lib.parse(sys.stdin, namespaceHTMLElements = False)
doc = ElementWrapper.from_html_root(t)

sel = "a.yt-uix-sessionlink"
links = list(doc.query_all(sel))
for link in links:
	href = link.etree_element.attrib.get("href")
	href = urljoin(base_url, href)
	seen[href] = True

hrefs = seen.keys()
hrefs.sort()
for h in hrefs:
	print h


