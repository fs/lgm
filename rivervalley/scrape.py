import subprocess, json

# use youtube-dl to dump jsons for each youtube URL
# show json in tablar or other editable form
#

urls = set()
with open("river_valley.urls") as f:
	for line in f:
		url = line.rstrip().split()[0]
		if url:
			out = subprocess.check_output(["youtube-dl", "--dump-json", url])
			data = json.loads(out)
			# for key, value in data.items():
			# 	print key, value
			d = {}
			d['url'] = url
			d['title'] = data.get("fulltitle")
			d['thumb'] = data.get("thumbnail")
			d['description'] = data.get("description")
			d['width'] = data.get("width")
			d['height'] = data.get("height")
			d['duration'] = data.get("duration")
			print json.dumps(d)

			break
