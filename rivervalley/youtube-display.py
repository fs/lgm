from __future__ import print_function
from argparse import ArgumentParser
import json, sys

p = ArgumentParser()
p.add_argument("input", nargs="*")
args = p.parse_args()
print ("""<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LGM youtube clips</title>
<link rel="stylesheet" href="youtube.css" />
</head>
<body>
""")

items = []

for i in args.input:
	print ("reading from {0}".format(i), file=sys.stderr)
	with open(i) as f:
		data = json.load(f)
		# print (data.get("fulltitle"), file=sys.stderr)
		items.append(data)


items.sort(key=lambda x: x.get("fulltitle", ""))

for i in items:
	print (u"""
<div class="item">
	<div class="caption">
	  <div class="title">{0[fulltitle]}</div>
	  <div class="description">{0[description]}</div>
	</div>
	<a href="{0[webpage_url]}"><img class="thumb" src="{0[thumbnail]}"></a>
</div>""".strip().format(i).encode("utf-8"), file=sys.stdout)

print ("""</body>
</html>""", file=sys.stdout)