from __future__ import print_function
from argparse import ArgumentParser
import json, sys

p = ArgumentParser()
p.add_argument("input", nargs="*")
args = p.parse_args()

items = []

for i in args.input:
	print ("reading from {0}".format(i), file=sys.stderr)
	with open(i) as f:
		data = json.load(f)
		# print (data.get("fulltitle"), file=sys.stderr)
		items.append(data)


items.sort(key=lambda x: x.get("fulltitle", ""))

for i in items:
	print (u"""{0[webpage_url]}
{0[fulltitle]}
{0[description]}
""".format(i).encode("utf-8"), file=sys.stdout)
