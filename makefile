
programs = 2007/program.html 2008/program.html 2009/program.html 2010/program.html 2011/program.html 2012/program.html 2013/program.html 2014/program.html 2015/program.html
program_microdata = $(programs:%.html=%.json)

pdf.js:
	git submodule init
	cd lib/pdf.js; \
	 npm install; \
	 node make generic

all: terms.json

microdata: $(program_microdata)

%.json: %.html
	microdata.py $< > $@

terms.json: microdata_append.py $(program_microdata)
	python microdata_append.py --output $@ $(program_microdata) 

# youtube-paths = $(shell python urlpaths.py cache/youtube.com )
# youtube-info = $(youtube-paths:%=%/info.json)
# youtube-json = $(youtube-paths:%=%/meta.json)

# 2014video-paths = $(shell python urlpaths.py cache/download.gimp.org )
# 2014video-originals = $(2014video-paths:%=%/original.ogv)

# all: $(youtube-json) youtube.html

# rivervalley: rivervalley/urls.txt
# 	python url2path.py --path cache $(shell cat rivervalley/urls.txt)

# 2014videos: 2014/video_urls.txt
# 	python url2path.py --path cache --source 2014/video_urls.txt

# 2014video-originals: $(2014video-originals)

# %/original.ogv: %/info.json
# 	wget "$(shell python jsonget.py --key url $<)" -O $@

# youtube-json: $(youtube-json)

# %/meta.json: %/info.json
# 	youtube-dl $(shell python jsonget.py --key url $<) --dump-json > $@

# youtube.html: $(youtube-json)
# 	python rivervalley/youtube-display.py $(youtube-json) > $@

# youtube.txt: $(youtube-json)
# 	python rivervalley/dumptext.py $(youtube-json) > $@


# all: lgm2014videos.urls lgm2014pdfs.urls

# lgm2014videos.urls: lgm2014videos.url
# 	crawl.py --mime video/ogg $(shell cat $<) > $@

# lgm2014pdfs.urls: lgm2014videos.url
# 	crawl.py --mime application/pdf $(shell cat $<) > $@

# lgm2015_scrape.json:
# 	python lgm2015_scraper.py > lgm2015_scrape.json

# lgm2015_scrape_stacked.json: lgm2015_scrape.json
# 	python stackstream.py lgm2015_scrape.json > lgm2015_scrape_flat.json

# lgm2015_scrape_flat.json: lgm2015_scrape_stacked.json
# 	python flattenstream.py lgm2015_scrape_stacked.json > lgm2015_scrape_flat.json

# %.html: %.md
# 	pandoc --from markdown --to html --self-contained $< -o $@

print-%:
	@echo '$*=$($*)'
