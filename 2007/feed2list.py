from xml.etree import ElementTree as ET 
import sys

t = ET.parse(sys.stdin)

for e in t.findall(".//enclosure"):
	print e.attrib.get("url").encode("utf-8")