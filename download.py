from __future__ import print_function
from urllib2 import urlopen
from argparse import ArgumentParser
from urlparse import urlparse
import sys, os


# p = ArgumentParser()
# p.add_argument("input", nargs="*")
# args = p.parse_args()

def humanize_bytes(bytes, precision=1):
    """Return a humanized string representation of a number of bytes.

    Assumes `from __future__ import division`.

    >>> humanize_bytes(1)
    '1 byte'
    >>> humanize_bytes(1024)
    '1.0 kB'
    >>> humanize_bytes(1024*123)
    '123.0 kB'
    >>> humanize_bytes(1024*12342)
    '12.1 MB'
    >>> humanize_bytes(1024*12342,2)
    '12.05 MB'
    >>> humanize_bytes(1024*1234,2)
    '1.21 MB'
    >>> humanize_bytes(1024*1234*1111,2)
    '1.31 GB'
    >>> humanize_bytes(1024*1234*1111,1)
    '1.3 GB'
    """
    abbrevs = (
        (1<<50L, 'PB'),
        (1<<40L, 'TB'),
        (1<<30L, 'GB'),
        (1<<20L, 'MB'),
        (1<<10L, 'kB'),
        (1, 'bytes')
    )
    if bytes == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if bytes >= factor:
            break
    return '%.*f %s' % (precision, bytes / factor, suffix)

def url2path (url, makedirs=True):
	p = urlparse(url)
	folder = p.netloc
	filename = os.path.split(p.path)[1]
	path = os.path.join(folder, filename)
	if not os.path.exists(path) and makedirs:
		try:
			os.makedirs(folder)
		except OSError:
			pass
	return path

while True:
	line = sys.stdin.readline()
	line = line.strip()
	if line == '':
		break
	if line:
		url = line
		outpath = url2path(url)
		fin = urlopen(url)
		with open(outpath, "wb") as fout:
			bytes = 0
			while True:
				data = fin.read(1024*500)
				if data == '':
					break
				fout.write(data)
				bytes += len(data)
				sys.stderr.write("\r{0} {1}   ".format(outpath, humanize_bytes(bytes, 0)))
				sys.stderr.flush()
			sys.stderr.write("\r{0} {1}   \n".format(outpath, humanize_bytes(bytes, 0)))

