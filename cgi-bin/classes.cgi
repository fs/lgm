#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import os, html5lib
from xml.etree import ElementTree as ET 
from urllib2 import urlopen
from urlparse import urljoin, urlparse
import urllib

fs = cgi.FieldStorage()
url = fs.getvalue("url", "")

# NEXT STEP: TURN THIS INTO A SIMPLE TEXT FILTER

def text_contents (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u''
    for child in elt:
        ret += text_contents(child)
        if child.tail != None:
            ret += child.tail
    return ret

path = None
if url:
    parts = urlparse(url)
    if parts.hostname == "localhost":
        path = parts.path.lstrip("/")

if path != None:
    f = open(path)
else:
    f = urlopen(url)

t = html5lib.parse(f, namespaceHTMLElements=False)

names = {}
by_class = {}

for elt in t.findall(".//*[@class]"):
    classList = elt.attrib.get("class", "").split()
    text = text_contents(elt).strip()
    if "name" in classList or "person" in classList:
        if text in names:
            names[text]['count'] += 1
        else:
            names[text] = {'elt': elt, 'text': text, 'count': 1}

    for c in classList:
        if c not in by_class:
            by_class[c] = {}
        objs = by_class[c]        
        if text in objs:
            objs[text]['count'] += 1
        else:
            objs[text] = {'elt': elt, 'text': text, 'count': 1}



print "Content-type: text/html; charset=utf-8"
print

classes = by_class.keys()
classes.sort()
for c in classes:
    objs = by_class[c]
    keys = objs.keys()
    keys.sort()
    print u"""<h2>{0}</h2>
    <ul>""".format(c).encode("utf-8")
    for k in keys:
        d = objs[k]
        print u"""<li>{0} ({1})</li>""".format(d['text'], d['count']).encode("utf-8")
    print """</ul>"""


# names_keys = names.keys()
# names_keys.sort()
# print """<h2>Names</h2>
# <ul>"""
# for n in names_keys:
#     d = names[n]
#     print u"""<li>{0} ({1})</li>""".format(d['text'], d['count']).encode("utf-8")


