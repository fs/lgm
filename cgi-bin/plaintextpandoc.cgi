#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import os, html5lib
from xml.etree import ElementTree as ET 
from urllib2 import urlopen
from urlparse import urljoin
import urllib
from subprocess import Popen, PIPE

fs = cgi.FieldStorage()
url = fs.getvalue("url", "")

# NEXT STEP: TURN THIS INTO A SIMPLE TEXT FILTER!!!

if url != "":
    cmd = "pandoc --from html --to plain".split()
    cmd.append(url)
    p = Popen(cmd, stdout=PIPE)
    out = p.stdout.read()
    print "Content-type: text/plain;charset=utf-8"
    print
    print out

