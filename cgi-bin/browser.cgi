#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import os, html5lib
from xml.etree import ElementTree as ET 
from urllib2 import urlopen
from urlparse import urljoin
import urllib

fs = cgi.FieldStorage()
url = fs.getvalue("url", "")

# NEXT STEP: TURN THIS INTO A SIMPLE TEXT FILTER

if url != "":
    f = urlopen(url)
    t = html5lib.parse(f, namespaceHTMLElements=False)

    for elt in t.findall(".//*[@href]"):
        # Absolutize the URL to the page url
        if elt.tag == "a":
            elt.attrib['href'] = "?url=" + urllib.quote(urljoin(url, elt.attrib.get("href")), safe="")
        else:
            elt.attrib['href'] = urljoin(url, elt.attrib.get("href"))

    for elt in t.findall(".//*[@src]"):
        elt.attrib['src'] = urljoin(url, elt.attrib.get("src"))

    print "Content-type: text/html; charset=utf-8"
    print
    print ET.tostring(t, method="html")

