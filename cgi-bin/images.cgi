#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import os, html5lib
from xml.etree import ElementTree as ET 
from urllib2 import urlopen
from urlparse import urljoin
import urllib

fs = cgi.FieldStorage()
url = fs.getvalue("url", "")

# NEXT STEP: TURN THIS INTO A SIMPLE TEXT FILTER!!!

print "Content-type: text/html; charset=utf-8"
print
if url != "":
    f = urlopen(url)
    t = html5lib.parse(f, namespaceHTMLElements=False)

    for elt in t.findall(".//img[@src]"):
        # Absolutize the URL to the page url
        src = urljoin(url, elt.attrib.get("src"))
        print """<img src="{0}" />""".format(src)
