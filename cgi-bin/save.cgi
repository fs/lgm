#!/usr/bin/env python

import cgi, json, os
import cgitb; cgitb.enable()
from urlparse import urlparse

fs = cgi.FieldStorage()
url = fs.getvalue("url")
resp = {}

if url:
	parts = urlparse(url)
	if parts.hostname == "localhost":
		path = parts.path.lstrip("/")
		if os.path.exists(path):
			os.rename(path, path+"~")
			resp['update'] = True
		text = fs.getvalue("text", "")
		with open(path, "w") as f:
			f.write(text)
		resp['msg'] = 'ok'

print "Content-type: application/json"
print
print json.dumps(resp)

