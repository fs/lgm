#!/usr/bin/env python

import cgitb; cgitb.enable()
import cgi

fs = cgi.FieldStorage()

n = fs.getvalue("n", "")
rel = fs.getvalue("rel", "")

print "Content-type: text/html; charset=utf-8"
print

print """<!DOCTYPE html>
<meta charset="utf-8" />
<head>
<title>{0[title]}</title>
<link rel="stylesheet" href="/index.css" />
</head>
<body>
<h1>{0[title]}</h1>
""".format({'title': n})
