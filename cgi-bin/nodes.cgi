#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import os, html5lib
from xml.etree import ElementTree as ET 
from urllib2 import urlopen
from urlparse import urljoin, urlparse
import urllib

fs = cgi.FieldStorage()
url = fs.getvalue("url", "")

# NEXT STEP: TURN THIS INTO A SIMPLE TEXT FILTER

def text_contents (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u''
    for child in elt:
        ret += text_contents(child)
        if child.tail != None:
            ret += child.tail
    return ret

path = None
if url:
    parts = urlparse(url)
    if parts.hostname == "localhost":
        path = parts.path.lstrip("/")

if path != None:
    f = open(path)
else:
    f = urlopen(url)

t = html5lib.parse(f, namespaceHTMLElements=False)

names = {}
by_class = {}

# make a list of tagged nodes
# where tags come from HTML elements (like p, u, em, ...)
# and/or classes

print "Content-type: text/html; charset=utf-8"
print

print """
<style>
.tags {
    width: 300px;
    float: left;
}
.item {
    border: 1px dotted gray;
}
</style>
"""
tags = {}
count = 0
felts = []

tag = fs.getvalue("tag")

for elt in t.findall(".//*"):
    if elt.tag not in tags:
        tags[elt.tag] = 1
    else:
        tags[elt.tag] += 1
    count += 1

    if tag and elt.tag == tag:
        felts.append(elt)

print "Total elements: {0}".format(count)
tagnames = tags.keys()
tagnames.sort()
print """<div class="tags">"""
print "<ul>"
for t in tagnames:
    print """<li><a href="?url={2}&tag={0}">{0} ({1})</a></li>""".format(t, tags[t], urllib.quote(url, safe=""))
print "</ul>"
print """</div>"""

if felts:
    print """<div class="felts">"""
    for elt in felts:
        print """<div class="item">"""
        print ET.tostring(elt, method="html")
        print """</div>"""
    print """</div>"""

