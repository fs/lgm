Day 1 – Tuesday May 10
======================

## 9:00 – 9:20 Opening Talk

* Louis Desjardins

## 9:30 – 10:00 Making color management “just work” using colord

* Richard Hughes

## 10:00 – 10:30 Connecting Device Calibration to ICC Profiles

* Kai-Uwe Behrmann

## 10:30 – 11:00 Plain Text: Graphic Design and Programming Culture

* Eric Schrijver

## 14:00 – 14:30 Using Blender for Patent Absurdity / advocacy animations

* Christopher Allan Webber

## 14:30 – 15:00 Krita: Professional Digital Painting

* Lukáš Tvrdý

## 15:00 – 15:15 Comic book drawing with Krita

* Timothée Giet

## 15:30 – 16:00**\
 **Lightning Talks**
-   *[Future Tools: The Libre Graphics Research
    Unit](../day-1.html#FemkeSnelting)*\
     Femke Snelting
-   *[Revivalism: open font conversion and other great
    awakenings](../day-1.html#NathanWillis)*\
     Nathan Willis
-   *[Vegesignes](../day-1.html#LeaurendLavieHyp-P%C3%A8re(Laval)Chabon)*\
     Leaurend Lavie Hyp-Père (Laval) Chabon

## 16:30 – 17:00 Freeing Fonts For Fun and Profit

* Dave Crossland

## 17:00 – 17:30 Unleashing the Power of Inkscape to Create and Share

* Brad Phillips


Day 2 – Wednesday May 11
========================

## 9:00 – 9:30 Libre Graphics Global Documentation Project

* Hong Phuc Dang

## 9:30 – 10:00 Free Documentation on Floss World

* Elisa de Castro Guerra

## 10:00 – 10:30 Crafting an Open Font Stack

* Christopher Adams

## 10:30 – 11:00 Libre Graphics magazine: A year of fantastic

* Ana Carvalho
* ginger coons
* Ricardo Lafuente

## 14:00 – 15:30 Autonomo.us Free Network Services and Global Conflicts

* Jon Phillips

## 15:30 – 16:00 Lightning Talks

    -   *[Sparkleshare](../day-2.html)*\
         pippin / Øyvind Kolås
    -   *[Laidout and Strange Interfaces](../day-2.html#TomLechner)*\
         Tom Lechner
    -   *[Efficient Creative Web Workflows – An
        Investigation](../day-2.html#IanScheller)*\
         Ian Scheller
    -   *[Instant VIPSMagick](../day-2.html#NicolasRobidoux)*\
         Nicolas Robidoux

## 16:30 – 17:00 Easy multi-projector desktop using Lighttwist compiz plugin

* Sébastien Roy

## 17:00 – 17:30 PiTiVi and the state of GStreamer video editing

* Jean-François Fortin Tam

## 17:30 – 18:00 Know-How to Share: Beyond Software and Arts

* Andreas Vox


Day 3 – Thursday May 12
=======================

## 9:00 – 9:30 Open Mind, Literally: Teaching Free Culture As A Life Goal, Brain Surgery, and A Networked Path to Recovery

* Pete Ippel

## 9:30 – 10:00 Data Viz: Open Source and Sources

* Stéphanie Vidal

## 10:00 – 10:30 Tau Meta Tau Physica – Open Source Digital Pattern Making software

* Susan Spencer Conklin

## 10:30 – 10:45 DeviantArt: Creating Community Around Creativity

* Deviantart
** Michael Halpert

## 10:45 – 11:00 OSP: Adventures in distributed design

* OSP

## 14:00 – 15:30 How to keep and make productive libre graphics projects?

* Ale Rimoldi
* Asheesh Laroia
* Bart Kelsey

## 15:30 – 16:00 Lightning Talks

    -   *[Laying out democracy](../day-3.html#AnaCarvalhoLT)*\
         Ana Carvalho, Ricardo Lafuente
    -   *Hypermedia and the Annihilation of Time and Space*
    -   *[How to make SVG speak spot color
        language](http://www.libregraphicsmeeting.org/2011/day-3#LouisDesjardins)*\
         Louis Desjardins
    -   *Maio nosso Maio*\
         Short film by Farid Abedelnour

## 16:30 – 17:00 A manhole to West Africa

* Magaouata Dan Bourgami
* Mamadou Diagne

## 17:00 – 17:15 Open Colour Standard: Dissipating the vapour

* ginger coons

## 17:15 – 17:30 Generative typesetting with Context

* John Haltiwanger

## 17:30 – 18:00 Print: The Final Frontier

* Jon A. Cruz


Day 4 – Friday May 13
=====================

## 9:00 – 9:30 Better and faster image resizing and resampling

* Nicolas Robidoux

## 9:30 – 10:00 MyPaint – the past, the present, and the future

* Jon Nordby

## 10:00 – 10:30 Quick and Dirty Usability: Leveraging Google Suggest to Instantly Know Your Users

* Michael Terry

## 10:30 – 10:45 Towards an Document Object Model (DOM) for Scribus

* Andreas Vox

## 10:45 – 11:00 Toonloop: animation for live performances

* Alexandre Quessy

## 11:00 – 11:20 Open Publishing with PressBooks

* Hugh McGuire & Christine Prefontaine

## 14:00 – 14:30 Scribus, O.I.F. branch

* Jean Ghali

## 14:30 – 15:00 Introducing AdaptableGIMP

* Michael Terry

## 15:00 – 15:30 Free Graphics Programming, experiences & thoughts

* Pierre Marchand

## 16:30 – 16:00 Lightning Talks

-   *[In situ animation with
    Toonloop](../day-4.html#AlexandreQuessyLG)*\
     Alexandre Quessy
-   *Pinpoint – A tool for making hackers do excellent presentations*\
     pippin / Øyvind Kolås
-   *Who needs the font menu anyway?*\
     Louis\_D / Louis Desjardins
-   *[The Natural Economics of Open
    Source](../day-4/index.html#ColinVernon)*\
     Colin Vernon
-   **16:00 – 16:30**\
     *Afternoon Break*

## 17:00 – 17:30 Fabricating a Libre Graphics Future

* Jon Phillips

## 17:30 – 18:00 Closing Talk

* Louis Desjardins

