from argparse import ArgumentParser
import json

p = ArgumentParser("")
p.add_argument("term")
p.add_argument("--index", default="../terms.json")
args = p.parse_args()

with open(args.index) as f:
	index = json.load(f)


index['terms']['args.term