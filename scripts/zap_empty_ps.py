from __future__ import print_function
from etutils import indent
import html5lib, sys
from argparse import ArgumentParser
from xml.etree import cElementTree as ET

p = ArgumentParser()
p.add_argument("--pretty-print", default=False, action="store_true")
p.add_argument("--verbose", default=False, action="store_true")
args = p.parse_args()

t = html5lib.parse(sys.stdin.read(), namespaceHTMLElements=False)


def process_node(t):
	for c in t:
		if c.tag == "p" and len(c) == 0 and (c.text or u"").strip() == "":
			if args.verbose:
				print ("ZAP", file=sys.stderr)
			t.remove(c)
		else:
			process_node(c)
process_node(t)


if args.pretty_print:
	indent(t)
print (ET.tostring(t, method="html"))
