import json, html5lib, urllib, re, sys
from xml.etree import cElementTree as ET
from argparse import ArgumentParser
from cssselect2 import ElementWrapper
from urlparse import urlparse


p = ArgumentParser("output talks in player ready format")
p.add_argument("--html", default="2015/program.html")
p.add_argument("--url", default="http://libregraphicsmeeting.org/2015/program/")
p.add_argument("--annotations", default="2015/annotations.json")
p.add_argument("--insert-after", default="h3.schedule_title")
p.add_argument("--pretty-print", default=True)
args = p.parse_args()

# with open(args.program) as f:
# 	program = json.load(f)
with open(args.annotations) as f:
	annotations = json.load(f)

with open(args.html) as f:
	tree = html5lib.parse(f, namespaceHTMLElements=False)
	doc = ElementWrapper.from_html_root(tree)

link_urls = {}
for item in annotations['items']:
	base = None
	urls = []

	for url in item['urls']:
		if url.startswith(args.url):
			base = url
		else:
			urls.append(url)

	if (base != None and len(urls) > 0):
		link_urls[base] = urls

def simple_css_to_etree_selector (s):
	ret = ".//" + re.sub(r"\.(\w+)", "[@class=\"\\1\"]", s)
	return ret

insert_after_selector = simple_css_to_etree_selector(args.insert_after)

from etutils import et_child_index, indent

def label_for_url (url):
	if re.search(r"\.((WEBM)|(MP4)|(OGV))$", url, re.I) != None:
		return "video"
	elif re.search(r"\.((PDF))$", url, re.I) != None:
		return "pdf"
	else:
		return "link"

for url in link_urls:
	# print url
	urlp = urlparse(url)
	# remember: urllib.unquote needs bytes (utf-8)
	selector = urllib.unquote(urlp.fragment.encode("utf-8")).decode("utf-8")
	# print "selector", type(selector), selector
	q = tree.find(u'.//*[@id="{0}"]'.format(selector))
	# q = doc.query('#' + selector.encode("utf-8"))
	if q == None:
		print "fragment not found", urlp.fragment
	ia = q.find(insert_after_selector)
	# print q, ia
	links = ET.Element("div")
	links.attrib['class'] = "medialinks"
	for url2 in link_urls[url]:
		# print "    ", url2
		link = ET.SubElement(links, "a")
		link.text = label_for_url(url2)
		link.attrib['href'] = url2
	index = et_child_index(q, ia)
	q.insert(index+1, links)


if args.pretty_print:
	indent(tree)

print ET.tostring(tree, method="html")

