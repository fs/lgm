import json
from argparse import ArgumentParser

p = ArgumentParser("output talks in player ready format")
p.add_argument("--program", default="2015/program.json")
p.add_argument("--annotations", default="2015/annotations.json")
args = p.parse_args()

with open(args.program) as f:
	program = json.load(f)
with open(args.annotations) as f:
	annotations = json.load(f)

urls = {}
def index (url1, url2):
	if (url1 not in urls):
		urls[url1] = []

for item in annotations['items']:
	for url in item['urls']:
		for url2 in item['urls']:
			if url != url2:
				index(url, url2)


talks = []

for item in program['items']:
	talk = {}
	talks.append(talk)
	plink = item['@id']
	talk['program_link'] = item['@id']
	talk['title' ] = item['properties']['title'][0]
	try:
		talk['presenter'] = item['properties']['presenter']
	except KeyError:
		pass
	# seek overlapping annotation
	for anno in annotations['items']:
		if plink in anno['urls']:
			for url in anno['urls']:
				if url.endswith(".pdf"):
					talk['pdf'] = url
				elif url.endswith(".ogv"):
					talk['video'] = url

print json.dumps({'talks': talks}, indent=2)
