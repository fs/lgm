from argparse import ArgumentParser
import html5lib, sys
from cssselect2 import ElementWrapper
from xml.etree import cElementTree as ET


def indent(elem, level=0):
    """ in-place pretty-printer from effbot.org """
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def sectionalize_node_on_br (parent, elt="div", eltclass="br"):
    new_content = []
    # div is the current wrap element
    div = ET.Element(elt)
    new_content.append(div)
    div.text = parent.text
    parent.text = None
    while len(parent):
        c = parent[0]
        parent.remove(c)
        if c.tag == "br":
            div = ET.Element("div")
            new_content.append(div)
            div.text = c.tail
        else:
            div.append(c)
    for div in new_content:
        if eltclass:
            div.set("class", eltclass)
        parent.append(div)

def process (node):
    brs = [c for c in node if c.tag == "br"]
    if (len(brs) > 0):
        sectionalize_node_on_br(node)
    else:
        for c in node:
            process(c)

p = ArgumentParser("")
args = p.parse_args()

t = html5lib.parse(sys.stdin.read(), treebuilder="etree", namespaceHTMLElements=False)

process(t)
indent(t)
print ET.tostring(t, method="html")


