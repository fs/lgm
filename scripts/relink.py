from __future__ import print_function
from argparse import ArgumentParser
from cssselect2 import ElementWrapper
import sys, html5lib, re, os
from urlparse import urlparse
import urllib
from xml.etree import cElementTree as ET 

p = ArgumentParser("remap links to local files base on filename")
p.add_argument("--path", default=".", help="file search path, default: .")
p.add_argument("--selector", default="a[href]", help="link selector (CSS), default: a")
p.add_argument("--filter", default=None, help="optional link filter (regex)")
p.add_argument("--pretty-print", default=False, action="store_true", help="pretty print")
p.add_argument("--verbose", default=False, action="store_true")
args = p.parse_args()
t = html5lib.parse(sys.stdin.read(), namespaceHTMLElements=False)
doc = ElementWrapper.from_html_root(t)

path = args.path.decode("utf-8")
files = []
for b, d, ff in os.walk(args.path):
	for f in ff:
		if (type(f) == str):
			f = f.decode("utf8")
		fp = os.path.relpath(os.path.join(b, f), args.path)
		files.append(fp)

def get_paths (p):
	ret = []
	for f in files:
		fn = os.path.split(f)[1]
		if fn == p:
			ret.append(f)
	return ret

for e in doc.query_all(args.selector):
	elt = e.etree_element
	href = elt.get("href")

	use = True
	if args.filter:
		m = re.search(args.filter, href)
		use = m != None
	if use:
		# http://stackoverflow.com/questions/16566069/url-decode-utf-8-in-python
		# to bytes, then unquote, result is utf8 str
		href = urllib.unquote(href.encode("utf8"))
		# back to unicode
		href = href.decode("utf8")
		# print href
		p = urlparse(href)
		_, p = os.path.split(p.path)
		# p = p.decode("utf-8")
		ff = get_paths(p)
		if len(ff) > 0:
			if (len(ff) == 1):
				lp = ff[0]
				if args.verbose:
					print (u"{0} ==> {1}".format(href, lp).encode("utf8"), file=sys.stderr)
				elt.set("href", lp)

if args.pretty_print:
	from etutils import indent
	indent(t)

print (ET.tostring(t, method="html"))

