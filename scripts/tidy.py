from etutils import indent
import html5lib, sys
from argparse import ArgumentParser
from xml.etree import cElementTree as ET

p = ArgumentParser()
p.add_argument("--pretty-print", default=False, action="store_true")
args = p.parse_args()

t = html5lib.parse(sys.stdin.read(), namespaceHTMLElements=False)

if args.pretty_print:
	indent(t)

print ET.tostring(t, method="html")
