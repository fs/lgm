from argparse import ArgumentParser
import re, json

p = ArgumentParser("Process a simple annotations file into JSON output")
p.add_argument("input", nargs="*")
p.add_argument("--format", default="json", help="output format, default: json")
args = p.parse_args()

def parse_annotations (f):
	anno = f.read().decode("utf-8")
	parts = re.split("\n\n+", anno)
	parts = [[y.strip() for y in x.split("\n") if y.strip()] for x in parts]
	return parts

if len(args.input) == 0:
	annotations = parse_annotations(sys.stdin)
else:
	annotations = []
	for path in args.input:
		with open(path) as f:
			annotations.extend(parse_annotations(f))

# print "{0} total assemblages".format(len(annotations))
# for p in annotations:
# 	print len(p)

output = {}
items = []
output['items'] = items

for urls in annotations:
	if len(urls) > 0:
		item = {}
		item['urls'] = urls
		items.append(item)


print json.dumps(output, indent=2)
