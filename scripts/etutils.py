
def et_child_index(elt, child):
    for i, c in enumerate(elt):
        if c == child:
            return i

def iterparent(tree):
    for parent in tree.getiterator():
        for child in parent:
            yield parent, child

def iterparentindex(tree):
    for parent in tree.getiterator():
        for i, child in enumerate(parent):
            yield parent, child, i

def indent(elem, level=0):
    """ in-place pretty-printer from effbot.org """
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def contents (e):
    ret = u''
    if e.text:
        ret += e.text
    ret += u''.join([ET.tostring(x) for x in e])
    return ret

def tag_open (elt):
    """ return a string of just the tap opener. handy for debugging """
    ret = u"<{0}".format(elt.tag)
    for key, value in elt.attrib.items():
        ret += u" {0}=\"{1}\"".format(key, value)
    ret += u">"
    return ret

# def text (e):
#     ret = u''
#     if e.text:
#         ret += e.text
#     ret += u''.join([text(x) for x in e])
#     return ret

def text (e):
    return ET.tostring(e, method="text", encoding="utf-8").decode("utf-8")
