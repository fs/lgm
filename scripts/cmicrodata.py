from __future__ import print_function
import html5lib
from xml.etree import cElementTree as ET
from argparse import ArgumentParser
import urllib, json


p = ArgumentParser("")
p.add_argument("input")
p.add_argument("--url", help="base URL")
p.add_argument("--output", default="json", help="output format (json, urls)")
args = p.parse_args()

items = []
with open(args.input) as f:
	t = html5lib.parse(f.read(), namespaceHTMLElements=False)
	#pathum-egodawatta-jumping
	for i in t.findall(".//*[@itemscope]"):
		_id = i.attrib.get("id")
		if _id:
			itemurl =u"{0}#{1}".format(args.url, urllib.quote(_id.encode("utf-8")))
		else:
			print ("WARNING: itemscope elt has no id: {0}".format(i), file=sys.stderr)

		if args.output == "urls":
			print (itemurl.encode("utf-8"))
			print ()

		else:
			item = {}
			item['@id'] = itemurl
			# gather meta
			values = {}
			for p in i.findall(".//*[@itemprop]"):
				pname = p.attrib.get("itemprop")
				pval = p.text
				if pname not in values:
					values[pname] = []
				values[pname].append(pval)
			item['properties'] = {}
			for key in values:
				item['properties'][key] = values[key]
			items.append(item)

if args.output == "json":
	print(json.dumps({'items': items}, indent=2))
