import html5lib, re
from argparse import ArgumentParser
from cssselect2 import ElementWrapper
from xml.etree import ElementTree as ET 


"""
        <div class="schedule_item schedule_item_presentation">
        <p class="schedule_speaker"><a name="#pathum-egodawatta-jumping"></a>Pathum Egodawatta</p>

=>
modernize the anchors

        <div class="schedule_item schedule_item_presentation" id="#pathum-egodawatta-jumping">
        <p class="schedule_speaker">Pathum Egodawatta</p>

"""

p = ArgumentParser()
p.add_argument("--input", default="program.html")
args = p.parse_args()


def et_child_index(elt, child):
    for i, c in enumerate(elt):
        if c == child:
            return i

with open(args.input) as f:
    tree = html5lib.parse(f.read(), namespaceHTMLElements = False)
    doc = ElementWrapper.from_html_root(tree)
    for div in doc.query_all("div.schedule_item"):
        p = div.query("p.schedule_speaker")
        if p:
            a = p.query("a")
            if a:
                anchor = a.etree_element.get("name")
                if anchor:
                    div.etree_element.set("id", anchor.lstrip("#").strip())
                p.etree_element.remove(a.etree_element)
                p.etree_element.text = (p.etree_element.text or u"") + (a.etree_element.tail or u"")

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

indent(tree)

print ET.tostring(tree, method="html")
