import html5lib, re
from argparse import ArgumentParser
from cssselect2 import ElementWrapper
from xml.etree import ElementTree as ET 
from itertools import tee, izip_longest


"""

nb: because of difficulty of getting it right, the last div is not wrapped around it's contents and should be manually wrapped.

"""

p = ArgumentParser()
p.add_argument("--input", default="program.html")
args = p.parse_args()


def pairwise(iterable):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        a, b = tee(iterable)
        next(b, None)
        return izip_longest(a, b)

def is_time_p (p):
    if p.etree_element.text:
        m = re.search(r"^\d?\d\:\d\d", p.etree_element.text)
        return m != None

def et_child_index(elt, child):
    for i, c in enumerate(elt):
        if c == child:
            return i

with open(args.input) as f:
    tree = html5lib.parse(f.read(), namespaceHTMLElements = False)
    doc = ElementWrapper.from_html_root(tree)
    time_ps = [x for x in doc.query_all("p") if is_time_p(x)]
    for p, nextp in pairwise(time_ps):
        elt = p.etree_element
        parent = p.parent.etree_element
        index = et_child_index(parent, elt)
        # print elt, parent, index
        # collect elements from p up to not including nextp
        run = []
        i = index
        while i<len(parent) and (nextp != None and parent[i] != nextp.etree_element):
            run.append(parent[i])
            i += 1
        # print len(run)

        # insert div and add run elements to it
        div = ET.SubElement(parent, "div")
        # shift position to (before) child
        parent.remove(div)
        parent.insert(index, div)

        div.set("itemscope", "itemscope")
        for r in run:
            parent.remove(r)
            div.append(r)

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

indent(tree)

print ET.tostring(tree, method="html")
