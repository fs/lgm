
Thursday 27 May 2010
=====================

## 09:00 Welcome to LGM 2010

* Louis Desjardin
* Femke Snelting
[tv](http://river-valley.tv/welcome-to-lgm-2010)

## 09:30 Extending Python for Speed

* Martin Renold

[tv](http://river-valley.tv/extending-python-for-speed)

## 10:00 A first outline for a UI for a fully GEGLed GIMP

* Peter Sikking

[tv](http://river-valley.tv/a-first-outline-for-a-ui-for-a-fully-gegled-gimp)

## 10:30 The Nathive image editor, its plugin system and some Python-based implemented technologies

* Marcos Diaz

## 11:00 Writing GIMP scripts and plugins

* Akkana Peck

[tv](http://river-valley.tv/writing-gimp-scripts-and-plugins)

## 11:30 WORKMEETINGS, BOF, LUNCH, WORKSHOPS

## 14:30 - 16:00 Short Talks

## Collaborative intellectual property

* Jose David Cuartas Correa

[tv](http://river-valley.tv/collaborative-intellectual-property)

## PureDyne
* Aymeric Mansoux
[tv](http://river-valley.tv/puredyne)

## UpStage - an open source web-based platform for cyberformance

* Helen Varley Jamieson
[tv](http://river-valley.tv/upstage-an-open-source-web-based-platform-for-cyberformance)

## Open Source project to enable fashion design using open data formats

* Susan L. Spencer
[tv](http://river-valley.tv/open-source-project-to-enable-fashion-design-using-open-data-formats)

## The Rural Design Collective Summer Mentoring Program

*  Rebecca Hargrave Malamud
[tv](http://river-valley.tv/the-rural-design-collective-summer-mentoring-program)

## How people use open source graphics in China and a book about the LGM
* Wang Lingzheng


## 16:30 Baroque Dreams: Live Multimedia Performance, Interpretive Culture, and Open Source Software

* 16:30      Barry Threw



  17:00 –\   Lila Pagola\
  17:30      **[Using and teaching free software, being a final user and not dying in the attempt](index.php-p=en|talk|29_break.html)**
## 17:30 Designing with Free tools in an Open Community: experiences from the Fedora Design Team

* Nicu Buculei, Martin Sourada


## 18:00 How to get contributors to your Free/Libre/Open Source project from Vietnam and Asia

* Hong Phuc Dang


## 18:30 How to Run an Art School on Free and Open Source Software

* Florian Cramer, Aymeric Mansoux, Michael Murtaugh



Friday 28 May 2010
===================
## 09:00 Diffusion Curves in Inkscape vector drawings

* Jasper van de Gronde


## 09:30 GNU LibreDWG - a free software library to handle DWG files from AutoCAD

* Felipe Corrêa da Silva Sanches


## 10:00 Device Colour Management

* Kai-Uwe Behrmann


## 10:30 Color Management and other new developments in Ghostscript

* Hin-Tak Leung


## 11:00 Best practices for designing/releasing/maintainin g/packaging open fonts

* Nicolas Spalinger


## 11:30 BOFs, WORKSHOPS, MEETINGS, LUNCH

* –


  14:30      

  14:30 –\   Short Talks
## 16:00 Styling TeX documents with Batch Commander

* Ricardo Lafuente



             OSP\
             **[Another year of Open Source Publishing](index.php-p=en|talk|osp.html)**

             Denis Moyogo Jacquerye\
             **[Font design and features for African languages](index.php-p=en|talk|font_design_africa.html)**

             Dave Crossland\
             **[Google's Font Initiative](index.php-p=en|talk|font_google.html)**

             Eric Schrijver\
             **[Content centric architecture and distributed versioning](index.php-p=en|talk|distributed_versioning.html)**

             Stani Michels\
             **[Phatch](index.php-p=en|talk|phatch.html)**
## 16:00 BREAK

* –


## 16:30 Elements of Typographic Freedom: Open Sources of Extraordinary Design

* 16:30      Christopher Adams


## 17:00 Designing a Better Tomorrow: How design is informed by metaphors, images and associations of social progress

* Mirko Tobias Schaefer


## 17:30 Cantarell: Designing Typefaces Using Only Free Software

* Dave Crossland


## 18:00 sK1 Project: Past, Present and Future

* Igor Novikov


## 18:30 Laidout and Desktop Publishing

* Tom Lechner


## 19:00 LGM DINNER

* –


  22:00      

  22:00      **[StatusCheck Brussels + REJON Version 31 Birthday Release Party](index.php-p=en|talk|jon.html)**
  ---------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Saturday 29 May 2010 {style="margin-top:1em; margin-bottom:.5em;"}
--------------------
## 10:00 Writing brush engines for fun and profit!

* Lukáš Tvrdý


## 10:30 Icon Workflows with Inkscape

* Jakub Steiner


## 11:00 Generative Node-based Design With NodeBox 2

* Frederik De Bleser, Tom De Smedt, Lucas Nijs


## 11:30 Scribus

* Peter Linnell


## 12:00 Blender Foundation, past and future

* Ton Roosendaal



  12:30      **[BOFs, WORKSHOPS, MEETINGS, LUNCH](index.php-p=en|talk|29_lunch.html)**

  15:30 –\   Short Talks
## 17:00 How I made a free New Zealand coffee-table book using lots of Free Software

* Marcus Holland- Moritz



             Ricardo Lafuente\
             **[Coding pictures with Shoebot](index.php-p=en|talk|shoebot.html)**

             Pete Ippel\
             **[The New Folk Tradition: Aesthetic and Community Resonance between Open Source Graphics and Fiber Arts](index.php-p=en|talk|aesthetic.html)**

             Steve Conklin\
             **[Multi-touch support in Ubuntu](index.php-p=en|talk|multitouch.html)**

             Alexandre Prokoudine\
             **[Digital photography workflow on Linux with darktable](index.php-p=en|talk|digital_photography.html)**

             Ana Carvalho\
             **[Plana, publishing illustration and comics with floss](index.php-p=en|talk|plana.html)**
## 17:30 Mozilla Open Web Graphics

* Paul Rouget


## 18:00 The Open Colour Standard: Physical colour for F/LOSS

* ginger coons


## 18:30 Inkscape for everybody

* Andy Fitzsimon


## 19:00 BREAK

* –


## 20:30 Durian Open Movie sneak peeks

* 20:30      Blender team



Sunday 30 May 2010
=====================
## 10:00 Graphic Design and the Wide Open Space

* Eric Schrijver


## 10:30 Viaduct - connecting apps and resources in a user friendly way

* Jon A. Cruz


## 11:00 Joining Spirits with Aiki Framework: The New Web Engine for Open Clip Art Library

* Bassel Safadi, Jon Phillips


## 11:30 Open Source Graphics and Web Services: A panel discussion

* Nathan Willis


## 12:30 LUNCH

* –


## 13:30 CLOSING

* 13:30      Discussion


