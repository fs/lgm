Wednesday May 2nd
=================

## 09:00 Opening Talk

* Louis Desjardins


## 09:30 ColorHug - The Open Hardware Colorimeter

* Richard Hughes


*The ColorHug was announced in November 2011 as a 100% open source
colorimeter device. The first 50 prototypes were lovingly hand soldered
by myself and assembled by my wife in our spare bedroom. Since then,
we've shifted from small hand-made batches to automated machines and
started making production quantities. The community has welcomed the
open hardware, with a vibrant community fixing bugs and adding new
features and the project has blossomed into a healthy ecosystem.\
\
 This is my first foray into producing open source hardware, and in this
talk I'll explain the first-hand experienced intracacies of setting up
an open hardware company, and what direction I see the project going in.
We'll cover topics like automated firmware upgrades and complicated
things like CCMX files, but without getting too bogged down with all the
calculations. I'll show lots (and lots) of pictures and will try to
avoid writing any words in my slides. I'll show all stages of
development, from breadboard to automated pick and place machines, along
with all the new software UIs and try to keep the talk lighthearted and
fun.\
\
 There will be time left for questions and discussion.*

## 10:00 Evolving Concepts for Colour Management

* Kai-Uwe Behrmann


*Many people interested in colour are taking part on OpenICC and think
about getting Linux CM right or at least better. The talk will summarise
some of the recent discussions.*

## 10:30 Color Management in SVG2

* Chris Lilley


*SVG 1.1 is now widely implemented, and has some (minimal, optional)
support for ICC colours. In the main though it is an sRGB system. SVG2
adds a conformance class for full ICC support (v2 and v4) including
named colours, direct specification in LAB or LCHab, and interpolation
in LAB or LCHab. Unmanaged device-specific colour is also supported.*

## 11:00 Colour Management a la Greek

* Kai-Uwe Behrmann


*The talk will dive into the usability and technical concepts of a cross
platform colour management system and comment on some myths and facts
around it.*

## 11:30 colord - Linux Color Management Framework, One Year On

* Richard Hughes
\

[Slides](http://libregraphicsmeeting.org/2012/program/FAILED%20(Temporary%20file%20could%20not%20be%20copied.))

*The colord project has had an amazingly successful year, and is now
installed by default on the Fedora, OpenSuse, Ubuntu and other
distributions.\
\
 In this talk I will quickly introduce why color management is important
and then outline the many new features that we've introduced over the
last 12 months, including automatic device to profile matching, native
sensor drivers and the new profile metadata that is supported by GNOME
Color Manager and some other tools. I will outline the future direction
of colord, and hopefully try to explain my vision of a color management
framework that "Just Works".\
\
 There will be time left for questions and discussion.\
*

## 12:00 Taxi DB - Call A Cab To Bring The Colors

* Sirko Kemter


*Taxi DB - the ICC profile database\
\
\
 In the past few years, color managment on free desktops made some huge
steps forward. The dream is to get a desktop that is color managed and
easy for users to handle: for example, if the user plugs in new
hardware, such as a projector, the system should automatically retrieve
the necessary ICC profile. For this goal to be possible ICC profiles
must be available, preferably in a centralized place where the color
managment system can find them: this is what Taxi DB does. Taxi DB was
first implemented during GSoC 2011 and is now available under
http://icc.opensuse.org/. DispcalGUI is integrated now as well; it can
upload to the profile database. In this talk, we'll talk about how to
use Taxi DB and the project's current status. 8*

## 15:00 Re-lab project. Formats reverse engineering: tools and results.

* Valek Filippov


*Insight on re-lab's reverse engineering tools and their usage to
implement/improve support for proprietary file formats in your program.\
 Progress report on latest achievements.*

15:30\
**Import filters for vector graphic formats in LibreOffice: the reverse-
and straight engineering fun**\
*Fridrich Strba*

*The talk aims at presenting the recent graphic import filters additions
to LibreOffice. It will show the feature coverage and the way we
developed them. The method allowed us to do quite substantial progress
in short time. The APIs used will be also presented briefly.*

## 16:00 sK1 Project Reloaded

* Igor Novikov


*sK1 project has been a bit silent for the most of this year. This is
basically because we started it all over again. A year ago we decided to
introduce major changes to both sK1, the vector graphics editor, and
UniConvertor, the vector graphics conversion tool. After some analysis
the decision was made to start everything from scratch although
initially the plan was to do refactoring of both sK1 and UniConvertor.\
\
 This decision didn't come out of blue, because the source code
inherited the architecture from Sketch/Skencil which was fine for late
90s, but didn't really resolved development issues for 2010s. Given
amount of code base to go through we decided to just redo everything
from start. That wasn't by any means an easy decision to make, but it
was the only way out, especially since supporting the old toolkit was
starting to take too much time. Mere 2-3 years ago we still had some
hopes for Tcl/Tk 8.5-8.6, but alas it was a defeat. So we also had to
find a new toolkit, and the choice was between GTK+ and Qt. Finally we
have selected Gtk widgetset. Architecture was another issue we had to
deal with, since we used to support sK1 and UniConvertor as two separate
projects using duplicated code base. Also we designed a new file format
appropriated to new document model that is modeled after OpenDocument
and called it PDXF (PrintDesign XML Format). Just like OpenRaster or
OpenDocument files it's a ZIP archive with content.xml that describes
the structure and nested folders for each type of embedded data.\
\
 As a result of deep project refactoring we are going to announce
releases of three new or renewed applications:\
 \* UniConvertor 2.0 - deeply refactored version of UniConvertor 1.x
branch\
 \* PrintDesign - succeeder of sK1 vector graphics editor\
 \* LinCuttor - new interactive tool for cutting plotters (something
like Roland Studio)*

## 17:00 GLyphy, a GPU-accelerated text rendering engine

* Behdad Esfahbod


*GLyphy is a new, OpenGL-based, text rasterizer that I have been working
on.  In this first public presentation about GLyphy I will discuss the
design, challenges, and benefits of such a approach to text rendering.
 The algorithms involved may prove to be useful in other computer
graphics contexts as well.*

## 17:30 World Welder: Solid Modeling from a New Perspective

* Daniel Phillips


*World Welder is a solid modeler project based on the new "advanced half
edge mesh" technology, World Welder introduces Ring Modeling, a powerful
technique suitable for either interactive or automated modeling. Tightly
coupled to a real time simulation engine with C/C++plugins. Lua
scripting to automate modeling. Multiple viewports. GLX or optional QT
host with dynamic widgets. Mesh compiler to optimize rendering. CAGL
interface. Computational geometry kernel includes subdivision surfaces,
mesh/plane booleans, mesh hit detection, contour maps, mitre joints,
bevels, much more. Written in C++11. Currently under heavy development;
now capable of generating various 3D media assets online or offline;
heading towards rich subdivision modeling and animation support.
License: GPLv3. Now inviting participation from interested developers.*

Thursday May 3rd
================

## 10:00 The awesome things that libre web type enables you to do

* Ricardo Lafuente
* Ana Carvalho


*Last year, we were still celebrating the appearance of @font-face,
allowing for the first time the use of non-standard typefaces on web
pages. The explosion of libre work in typography thanks to initiatives
such as the Open Font Library, the League of Movable Type or Google Web
Fonts has been remarkable.\
 At the same time, many other efforts appeared in order to deal with
finer points in the challenge of good, elegant web typography,
particularly in the forms of JavaScript libraries such as Lettering.js,
Kerning.js or Colorfont.js. Recent projects like Flossmanuals's BookType
propose to close the loop, generating fully typeset PDF documents out of
collaboratively edited online books managed through a simple CMS. And we
won't forget the enormous potential of Python scripts made to work with
typefaces using Fontforge. It is an exciting time for type designers and
typographers -- especially if you're on the libre side of things!\
 Ana and Ricardo from Manufactura Independente will drive you through
the history and recent developments in libre typography and type design,
while providing many hints, technical hacks and advice on how to make
the most of your designs; there will be a special focus on where and why
using fonts and software that respect your freedom can elevate your
design practice.*

## 10:30 I script design and so can you

* jeroen dijkmeijer


*iScriptDesign lets you create images in an untraditional way: "Thou
shalt script".With a script in your sketch, everyone can experience
interactive, animatable and parametric online graphics. And that's only
the start! Imagine online parametric blueprints for 3d printing, or cnc
routing. Instead of carving dimensions in rock, let the customer adapt
it to his needs.Using iScriptDesign you'll never ever experience a
leaving customer who is in love with your design but not with its the
dimensions.You will see how iScriptDesign not only solved some of my day
to day problems, but how it can also solve your problems as well.*

## 11:00 Digital Painting with open source.

* Ramon Miranda


*My talk is about what we already have in opensource, like Gimp,
Mypaint, Krita... and has videos, examples of works. and a
presentation.\
 Is not attached to an specific software and is for mid-users Is about
Creative proccess and how we can explore ideas with the software.\
*

## 11:30 Predictable Painting

* Martin Renold


*When painting details, or when brightening up an area with a brush, a\
 digital artist should be able to predict the effect of a tablet's\
 stroke. Some of the typical workflows in GIMP, Krita and MyPaint are\
 technically correct, but not quite predictable or expected.\
\
 I explain what the problems are, from my point of view. They are\
 subtle, and some of you may disagree. Often, workarounds are\
 available, but they are not always obvious or convenient.\
\
 This talk is a summary of a past and ongoing discussion.\
*

## 11:40 Laidout

* Tom Lechner


*Current state of Laidout, including various new on canvas tools, and
3-d shapes.*

## 11:50 Elektra - A Modular Approach to Configuration Storage

* Markus Raab


*Most software write their own configuration library\
 to store and read settings or parameters. This leads\
 to a system which is not well integrated.\
\
 I will present a working solution - Elektra - which\
 overcomes this problem but leaves full control over\
 the configuration to the programmer and the administrator.\
\
 Elektra is already used in the oyranos color management\
 and I hope that some other programmers will also join to\
 use it because of its modular approach which leads to benefits\
 including type checks, validation, structure checks, logging,\
 notification and changing character encoding without any\
 additional effort.\
\
 For more information see:\
 http://www.markus-raab.org/ftp/elektra/poster.pdf\
 http://www.markus-raab.org/ftp/elektra/thesis.pdf*

## 13:00 (workshop) Hugin's Eye / Panorama Workshop

* Kai-Uwe Behrmann


*Let's broaden the view of your digital camera. We will do some
beginners steps with our own cameras and the well known panorama
software and get around the most common pitfalls. Tripod is helpful.*

## 13:00 (workshop) Weird Layout

* Tom Lechner


*This workshop is to discuss and practice laying out graphics on various
things that are easily made physically, whether books or 3-d shapes.
Will demonstrate with a combination of Laidout, Polyptych, and other
programs to help at various stages.*

## 13:00 (meeting) SANE and/or scanner support

* Nathan Willis


*A roundtable discussion about SANE and/or scanner support this year,
about whether not it needs fixing, a whole bunch more\
 personpower, or even outright replacement?\
\
 It's just something I hear come up a lot, usually not in a positive
list, and not infrequently in close proximity to expletives. Obviously
there are other application types that rely on SANE or scanner access in
general (e.g., OCR) that would have different requirements, but the
graphics projects certainly deal with it more frequently. My apologies
if you're a huge SANE fan, but I see a lot of individuals complaining
about different aspects of the design and the API -- it probably can't
hurt to have that discussion in one place and see what comes out of it.*

## 14:00 (meeting) GUI of Scribus and effectiveness of work

* Scribus Team


*Discussing with the Scribus team about the effectiveness of the GUI*

## 16:00 HarfBuzz, the Free text shaping engine

* Behdad Esfahbod


*HarfBuzz is the Free Software text shaping engine.  It is currently in
use by GNOME, KDE, Firefox, Chrome, and Android among others.  A
collaborative effort was started years ago to rewrite HarfBuzz for
better stability, performance, and maintainability.  The effort has been
know as harfbuzz-ng.\
\
 I this session we will present the current status of harfbuzz-ng,
what's been done, what still needs to be done, and what the challenges
are. Design and performance considerations will be discussed.*

## 16:30 An awesome FLOSS design collaboration workflow

* Máirín Duffy
* Emily Dirsh


*Free software designers don't have a great set of integrated tools to
work with to collaborate with each other and with the community on
designs. We've come farther since the GNOME UX hackfest in 2010; let's
make more progress towards fixing that. We'll review and demo the
existing tools we do have, talk about their current status. and we'll
also present our ideas for tool integration and further expansion and
new tools.\
\
 Some of the tools and ideas we'll cover include:\
\
 (1) Sparkleshare: a git-backed, Dropboxlike system that automatically
checks in and pushes files to a shared git repo.\
\
 (2) Magic Mockup: a coffeescript/javascript you can insert into an SVG
to enable interactive, click-through navigation functionality for
mockups.\
\
 (3) Design Hub: an idea and a rough ruby on rails prototype of a web
interface that could potentially serve as a front end to
Sparkleshare-backed git repos with design assets as well as serve as a
front end to magic mockup assets.\
\
 (4) Inkscape: We have many integration ideas for workflows between
Inkscape's GUI and Magic Mockup, Sparkleshare, and Design Hub.*

## 17:00 The importance of new tools to enable collective design process

* Silvia Schiaulini


*With this talk I want to underline the importance of collective design
process and to point out some proposal for the development of tools that
enable the collective design of communicative artefacts. \
 The study of popular spontaneous collective processes could be an
important element for the development of enabling tools for collective
design. The online community has find out many solution to overpass
technical restraint linked with co-creation (e.g. Youtube and the
difficulties of re-editing videos by other users), but still there is a
lot to do for helping this need of collective creation.\
 A lot of other insights could be obtained from the notes of tutors and
designers during workshops or teaching experiences: from real problem to
interesting solutions. \
 The ideas exchange between software developers and designers  can help
developing solutions to encourage people to construct a collective
discourse more than an individual monologue.*

## 17:30 Re-Imagining UpStage

* Helen Jamieson
* Martin Eisenbarth
* Jenny Pickett


*UpStage is an online platform for the real-time collaborative
manipulation of digital graphics and other media into live performances
for online audiences. The original software is now 8 years old, and
during the last year developer Martin Eisenbarth has proposed a base
concept and software architecture for a new engine, called DownStage,
that will drive UpStage and potentially other variations of the platform
as well. As UpStage is an unfunded and independent artists' project, we
are seeking ways to support this development, and one source of support
has come through a collaboration with the French digital artists'
collective APO33. \
\
 In this talk Helen will give a brief introduction to UpStage, then
Jenny will talk about APO33, their FLOSS developers' residencies and the
collaboration with UpStage. Martin will then introduce the new engine,
DownStage, and outline the ideas that motivate it, which include aiming
for a modular and extensible structure that will more easily enable
other developers to get involved. *

## 18:00 Lightning performance

* Helen Jamieson, remote performers will be contributing


*A 5 minute "lightning performance" to demonstrate cyberformance using
the online platform UpStage, featuring remote performers.*

Friday May 4th
==============

## 10:00 Powerstroke: variable stroke width in Inkscape

* Johan Engelen


[Slides](../wp/wp-content/uploads/gravity_forms/3/2012/05/LGM2012_-_Powerstroke.pdf)

*With the advent of Live Path Effects (LPEs) and especially the
pattern-along-path LPE, it became possible to edit a path's stroke width
live on-canvas. The pattern-along-path LPE was however not designed for
variable stroke width use, resulting in a very awkward UI experience.
Powerstroke is the name of an upcoming LPE dedicated to variable stroke
width.\
 The talk will start with a brief introduction to LPEs, how they work
and what they can and cannot do, followed by a detailed discussion of
the new Powerstroke LPE.*

## 10:30 rethinking text handling in GIMP

* peter sikking
* Kate Price


*The role of text within GIMP work ranges far beyond the text tool.
However, the handling of text had never received any interaction design
love… that is, until the beginning of 2012.\
\
 That is when the GIMP UI design team ran a text handling design
project, where solutions were methodically designed based on the right
foundations.\
\
 Kate and Peter will show the steps the UI team took, and how much a
deep understanding of GIMP, GIMP work and users informed the work.
Besides that there will be plenty of take away points for other Libre
Graphics projects.*

## 11:00 Implementing OpenCL support in GEGL and GIMP

* Victor Oliveira


*In this session I'm going to describe some efforts to bring OpenCL\
 acceleration to the General Graphics Library (GEGL) and the GNU
Image Manipulation Program (GIMP).\
 I intend to show the current state of the project, some implementations
techniques used and performance comparisons among common GPUs.*

## 11:30 GeglBuffer tight and flexible raster abstraction.

* Øyvind Kolås


*GeglBuffer is a powerful framework providing a clean abstraction to
pixel storage. It has a small public API, architecture, GPU integration,
pluggable backends and extendable color models, pixelformats and
conversions using babl. Pluggable tile backends allows integrating
GeglBuffers transparently with on top of existing pixel buffer data
types, remote buffers, online map services or even to do on demand
fractal rendering.*

## 11:40 Goat Invasion in GIMP

* Michael Natterer


*A brief introduction about GEGL-based GIMP.\
\
 Without really planning for it, we happened to port the GIMP core to
GEGL. This talk will give an overview about some new concepts in the
GIMP core and on the plug-in side.\
*

## 11:50 Petting Zoo with Goats

* Øyvind Kolås


*Open discussion about the goat invasion in GIMP*

## 13:00 (workshop) Colorfont workshop

* Ana Isabel Carvalho
* Ricardo Lafuente


*Colorfont.js is a jQuery script we developed to allow multi-coloured
typography on the web. Along with the script, we designed a set of
alternates to existing libre fonts for use with this library.\
 One approach to create coloured type is to make variations around
existing libre fonts. These variations are intented to use as overlays
to the original, extensions to existing typefaces.A project such as this
is only possible in the context of libre fonts and libre graphics, since
creating and redistributing modified versions of fonts is a fundamental
premise. In this spirit we propose a workshop around multi coloured
type.\
 We'll start with an overview of coloured type in graphic design, in
order to collectively plan and sketch a set of new colorfonts to be
created on the workshop. From there, we'll be making good use of
Fontforge, Inkscape, Python scripting and Git version control in order
to collaboratively build and release new designs for use with
Colorfont.js (and many other tools!).*

## 13:00 (meeting) Using CREATE's web presence as a resource site

* Nathan Willis


*Using CREATE's web presence as a resource site, both for developers as
they discover the creative app universe and for artists/designers
interested in learning more. It would at least be worth a site hacking
session at LGM this year. What CMS/framework is used for that is
probably not as important as getting the content right, of course.*

## 13:00 DownStage Work Session

* Helen Jamieson
* Martin Eisenbarth


*This should happen after the "Re-imagining UpStage" presentation: a
work session where Martin will explain DownStage in more detail and
interested people will be invited to brainstorm topics including:
setting up an environment for development; running prototype basics
(XMPP real-time messaging, video-streaming, REST-API access using
different programming languages); and discussion about development,
design considerations and uses of the platform.*

## 14:00 (workshop) Lib2Geom: awesome geometry calculations made easy

* Johan Engelen


*A workshop explaining how to program using lib2geom, the geometry
framework library that is used by Inkscape. I will briefly introduce the
main lib2geom concepts, and show many examples of how it is used in
Inkscape. After that, I'll try to answer any questions.*

## 14:00 (workshop) ICC Rendering Intents

* Kai-Uwe Behrmann

This is the request for a BoF/Workshop.


*Visualise and explain the effect of rendering intents as well as
proofing profiles.*

## 15:00 Doing Design In The World Of FLOSS

* Sirko Kemter
* Máirín Duffy
* Allan Day


*Free and Open Source Software lives via the collaboration of many
people with different views and opinions on things. Art and design work
can also be created in a collaborative manner. Working together on art
and design work for free and open source software can seem an impossible
thing to do, however. This talks will explain how art and design work is
done in the context of particular FLOSS projects and the types of
challenges free and open source design contributors face.*

## 17:00 Tools shape practice, practice shapes tools: The Libre Graphics
Research Unit

* Femke Snelting
* Marcos Garcia

*The work at the Libre Graphics Research Unit (LGRU) is now almost
halfway. Since June 2011, the Unit functions as a traveling lab where
new ideas for and about creative tools are developed. LGRU is an
initiative of four European media-labs actively engaged in Free/Libre
and Open Source Software and Free Culture.\
 With this presentation we would like to give you an overview of what
has happened sofar. More inportantly we would like to talk to you about
why and how we think LGRU can be of use for future Libre Graphics.*

## 17:30 Free Culture meets Free Software: Wiki Loves Monuments

* Nicu Buculei


*Wiki Loves Monuments is a photography contest organized by Wikimedia,
in 2011 it collected over 170000 freely licensed photos of hystorical
monuments from Europe. In 2012 it will go beyond Europe, with many other
countries participating.*

## 18:00 Tube Open Movie: Blender 3D Animation in a Distributed Pipeline

* Bassam Kurdali
* Fateh Slavitskaya


*Bassam demos the recent work of 3D animation group, URCHN, showing
through images and video how F/LOSS can be used to build ambitious films
organized around access rather than artificial scarcity. ‘Open Movies’
are a continuation of the free/libre open source development model into
the realm of media production. The open movie process delivers not just
a film, but an entire production pipeline using Blender and Python for
new custom tools, Mypaint, GIMP, Krita, Inkscape, Ardour, Ogg, Linux and
more. Elephants Dream, the original open movie Bassam directed, proved
it possible to make high quality 3D animation using libre tools in a
studio setting. The Tube Open Movie is a new experiment, this time in
distributed collaboration — a love letter to free software and open
culture that marks their convergence with independent filmmaking.*

Saturday May 5th
================

## 10:00 Krita:

* Lukáš Tvrdý
* Timothée Giet


*Lukáš and Timothée will introduce soon to be released Krita 2.4.
Timothée will practically demonstrate cool new features in Krita while
Lukáš will talk about technical side of those features. Main topics will
be what's new in Krita, what can it offer to professional painter and
what is still missing and has to be done. In the end we will show
stunning art made with Krita.*

## 10:30 Introducing Synfig Studio

* Timothée Giet


*Synfig studio is a cool vector animation software. Through a small
screencast, I want to show the basics of how to use it, and present some
fun new features. Also it will be a good occasion to talk about its
current development status (upcoming features, call for contributions,
collaborations with other projects…).*

## 11:00 Remake - build system for animation projects

* Konstantin Dmitriev


*In this talk I'm going to introduce Remake build system that is used to
maintain rendering for animation projects created with Blender or Synfig
Studio. I will share some of my experience in animation projects
development, outline the problem of rendering complex projects and
demonstrate Remake as solution (with live examples).\
 Remake is an opensource software. Homepage:
http://morevnaproject.org/tag/remake/.*

## 11:30 Coding for Synfig: Our road ahead

* Diego Barrios Romero


*In this presentation I will talk about the main challenges we are
facing in Synfig development and how we plan to solve them.\
 The points are:\
 Rendering speed\
 Bones support\
 Sound integration\
 Video import\
 UI redesign\
 Scripting support\
 Building system*

## 12:00 SoundFumble: live sound generation with Gimp

* Amir Hassan


## 13:00 (workshop) Krita workshop focused on illustration and comics

* Timothée Giet


*I want to make a workshop showing some workflow to create illustrations
and comics with Krita.*

## 13:00 (workshop) Open Source, Just Works - The LGM Book Workshop

* Sirko Kemter


*Free and open source software can used for everythin now. If you not
believe that, you should visit this workshop. Of course you should also
join it, if you like to learn using Krita, Mypaint, Gimp, Inkscape and
Scribus.\
 The target of the workshop is preparing a book with this tools. This
book will be printed and available on amazon, so what you produce in the
workshop will be in it.\
 There will be several working groups, you can join.\
 One group will prepare the cover using Krita, Mypaint and GIMP.\
 Another group will work with Inkscape, preparing diagrams and
illustrations for the book\
 and last but not least there will be a group working with Scribus
putting everything together, so that it becomes a book.*

## 13:00 Elektra: workshop on the Configuration Storage

* Markus Raab


*...*

## 14:00 (workshop) Development and pathology of Graphic design in Iran

* Behrouz Foladi


*Graphic design is an aggregative art and has a huge function in its
international field. But, in Iran there isn’t any correct understanding
of that because the different branches of this amazing art has not
separately been defined and utilized to make the people see and receive
its visual presence and affect in their life environments. The graphic
designer has to be like a working machine and does work in all of the
branches alone to propound the functions in society. Although the
designer knows all of these branches, such as designing a logo, they are
not supposed to be included in her work experience, because each of
those requires academic professional experience. On the other hand, the
communications has taken a special new visual form these days and
although the Iranian designers haven’t still reached such technical
development in the framework of graphic design profession, they have to
follow the global direction and use digital technology but, the presence
of digital technology has forced them to almost quit working with the
previous tools and equip themselves to this new one, because the
mentioned presence has been put into work before creating a suitable
ground of acceptance for it. This occurrence has made the situation
worse. Old hand designers have lost their jobs and even though just a
few of them could have learnt skills in this regard, the power of
computer is completely at the hands of young generation. Even though the
history proves that they could have connected themselves to the global
network, they have failed in some grounds. There still haven't been
stated different specialties and management graphic design careers as if
some people would like to call themselves “Art directors”. According to
my research, some solutions can be offered for this problem.*

## 16:00 Transitional Work Flow For Artists Making The Libre Graphics Leap

* Pete Ippel


*Many artists fear making a transition from proprietary software
platforms on which they have honed their skills into the world of Libre
Graphics software.  \
\
 In this talk I will outline a process to develop a complete studio work
flow around Libre software tools from project ideation to management to
production.  In addition archiving will be addressed.\
\
 In addition I will give examples on how to teach the workflow to your
artist peers and students.\
\
 Strategies and a time-line will be explained along with work examples.\
\
 I made the transition myself starting at LGM 2010 and want to share my
experience and knowledge with other creatives.*

## 16:30 It Builds Character

* Nathan Willis


*This talk is a tour of new open source software tools for font
development that have been released over the past year. Several
independent projects have seen the light of day during the previous 12
months, each adding different features to the arsenal of free software
utilities. The heavy hitters includes Google's sfntly, the typography
extensions for Inkscape, and newly GUI-empowered ttfautohint. We can
look at how each one contributes to the font development workflow, as
well as where they are headed next.*

## 17:00 Closing Talk

* Louis Desjardins


*Summary of this year's LGM and plans for next year's one.*

