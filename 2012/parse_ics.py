import sys, json

items = []

for line in sys.stdin:
	line = line.rstrip()
	if line.startswith("BEGIN:VEVENT"):
		curitem = {}
	elif line.startswith("SUMMARY:"):
		curitem['title'] = line.split(":", 1)[1].strip()
	elif line.startswith("DTSTART:"):
		curitem['dtstart'] = line.strip()
	elif line.startswith("DESCRIPTION:"):
		if "by" in line:
			curitem['presentor'] = [x.strip() for x in line.split("by", 1)[1].strip().split(",") if x.strip()]
	elif line.startswith("END:VEVENT"):
		items.append(curitem)

items.sort(key=lambda x: x.get("dtstart"))

json.dump(items, sys.stdout, indent=2)
