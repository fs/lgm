function controlswidget (elt, opts) {
	var that = {elt: elt},
		swap = document.createElement("button");

	swap.innerHTML = "swap";
	swap.addEventListener("click", function () {
		if (opts.swap) {
			opts.swap.call(that);
		}
	});
	elt.appendChild(swap);

	return that;
}