function jogplaybutton (elt, opts) {
	/*
	requires: interactjs.io
	*/
	// TODO: make draggable / resizable
	var that = {},
		canvas = document.createElement("canvas"),
		timecode = document.createElement("div");
	elt.appendChild(canvas);
	canvas.setAttribute("width", "220");
	canvas.setAttribute("height", "220");
	canvas.style.position = "absolute";
	canvas.style.top = "0px";
	canvas.style.left = "0px";
	canvas.style.zIndex = "1";
	elt.appendChild(timecode);
	timecode.classList.add("timecode");
	timecode.style.position = "absolute";
	timecode.style.top = "0";	
	timecode.style.left = "0";	
	timecode.style.zIndex = "0";	
	timecode.style.width = "220px";
	timecode.style.textAlign = "center";
	timecode.style.fontFamily = "monospace";
	timecode.style.fontSize = "18px";
	timecode.style.fontWeight = "700";
	timecode.style.textShadow = "0px 0px 2px white"

	function d2r (d) { return (Math.PI*2*(d/360)) }

	that.position = function (x, y) {
		if (x === undefined && y === undefined) {
			return {x: cx, y: cy}
		}
		cx = x; cy = y;
		draw();
	}

	that.playing = function (v) {
		if (v === undefined) {
			return playing;
		} else {
			playing = v;
			draw();
		}
	}

	that.currentTime = function (t) {
		if (t === undefined) {
			return curtime_seconds;
		} else {
			set_time(t);
		}
	}

	that.duration = function (t) {
		if (t === undefined) {
			return duration_seconds;
		} else {
			duration_seconds = t;
			duration = hmsfa(duration_seconds);
			draw();
		}
	}

	var ctx = canvas.getContext("2d"),
		cx = 110,
		cy = 110,
		bRadius = 50,
		sRadius = 120,
		mRadius = 83,
		mLineWidth = 32,
		sLineWidth = 4,
		curtime_seconds = 0,
		curtime = hmsfa(curtime_seconds),
		duration_seconds = 20 * 60,
		duration = hmsfa(duration_seconds),
		sda = ((sLineWidth/2) / sRadius),
		mda = ((mLineWidth/2) / mRadius),
		lightGrayStrokeStyle = "rgb(220,220,220)",
		grayStrokeStyle = "rgb(128,128,128)",
		mStrokeStyle = "rgb(0,0,0)",
		sStrokeStyle = "rgb(0,0,0)",
		dragging = 0,
		draggingStartAngle,
		TEN_DEGREES = (Math.PI*2) * (10 / 360),
		DRAGGING_SECONDS = 1,
		DRAGGING_MINUTES = 2,
		playing = false,
		drawOuterRim = false;

	function normalize (r) {
		while (r >= Math.PI) { r -= Math.PI*2; }
		while (r < -Math.PI) { r += Math.PI*2; }
		return r;
	}

	function hmsf (t) {
		var h, m, s, raw = t;
		h = Math.floor(t/3600); t -= (h*3600);
		m = Math.floor(t/60); 	t -= (m*60);
		s = Math.floor(t); 		t -= s;
		return { h: h, m: m, s: s, f: t, raw: raw };
	}

	function hmsfa (t) {
		// time (seconds) to angles hr, mr, sr in range -PI to PI and clock adjusted (12 o'clock is -PI/2)
		var ret = hmsf(t);
		ret.sr = normalize((-Math.PI/2) + (Math.PI * 2 * ((ret.s + ret.f) / 60)));
		ret.mr = normalize((-Math.PI/2) + (Math.PI * 2 * ((ret.m + ((ret.s+ret.f)/60)) / 60)));
		ret.hr = normalize((-Math.PI/2) + (Math.PI * 2 * ((ret.h % 12) / 12)));
		return ret;
	}

	function draw () {
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		// m
		ctx.lineWidth = mLineWidth;
		ctx.beginPath();
		ctx.strokeStyle = grayStrokeStyle;
		ctx.arc(cx, cy, mRadius-(mLineWidth/2), -Math.PI/2-mda, duration.mr + mda)
		ctx.stroke();
		ctx.beginPath();
		ctx.strokeStyle = mStrokeStyle;
		ctx.moveTo(cx, cy);
		ctx.arc(cx, cy, mRadius, curtime.mr, curtime.mr)
		ctx.stroke();

		// s
		ctx.lineWidth = sLineWidth;
		if (drawOuterRim) {
			ctx.strokeStyle = lightGrayStrokeStyle;
			ctx.beginPath();
			ctx.arc(cx, cy, sRadius-(sLineWidth/2), -Math.PI, Math.PI);
			ctx.stroke();
			// first second marker
			ctx.beginPath();
			ctx.strokeStyle = grayStrokeStyle;
			ctx.arc(cx, cy, sRadius-(sLineWidth/2), (-Math.PI/2)-sda, (-Math.PI/2)+sda)
			ctx.stroke();
			// last second marker
			ctx.beginPath();
			ctx.arc(cx, cy, sRadius-(sLineWidth/2), duration.sr-sda, duration.sr+sda)
			ctx.stroke();
		}

		ctx.lineWidth = sLineWidth+2;
		ctx.beginPath();
		// ctx.strokeStyle = sStrokeStyle;
		ctx.strokeStyle = "rgb(128,128,128)";
		ctx.moveTo(cx, cy);
		ctx.arc(cx, cy, sRadius+1, curtime.sr, curtime.sr)
		ctx.stroke();

		ctx.lineWidth = sLineWidth;
		ctx.beginPath();
		// ctx.strokeStyle = sStrokeStyle;
		ctx.strokeStyle = "black";
		ctx.moveTo(cx, cy);
		ctx.arc(cx, cy, sRadius, curtime.sr, curtime.sr)
		ctx.stroke();


		// Central play button
		ctx.fillStyle = playing ? "rgb(0,0,0)" : "rgb(255,255,255)";
		ctx.strokeStyle = "rgb(0,0,0)";
		// ctx.strokeStyle = playing ? "rgb(255,255,255)" : "rgb(0,0,0)";
		ctx.beginPath();
		ctx.moveTo(cx-bRadius, cy);
		ctx.arc(cx, cy, bRadius, -Math.PI, Math.PI);
		ctx.fill();
		ctx.stroke();

		ctx.fillStyle = playing ? "rgb(255,255,255)" : "rgb(0,0,0)";
		ctx.beginPath();
	    ctx.moveTo(cx-15,cy-30);
	    ctx.lineTo(cx+30,cy);
	    ctx.lineTo(cx-15,cy+30);
	    ctx.closePath();
	    // ctx.lineTo(cx,cy-30)
	    ctx.fill();

	    var t = curtime;
	    timecode.innerHTML = ((t.h<10)?"0"+t.h:t.h)+":"+((t.m<10)?"0"+t.m:t.m)+":"+((t.s<10)?"0"+t.s:t.s);
	}

	function set_time (t) {
		t = Math.max(0, Math.min(duration_seconds, t));
		curtime_seconds = t;
		curtime = hmsfa(t);
		draw();
	}
	set_time(curtime_seconds);

	function play() {
		// playing = !playing;
		if (!playing) {
			if (opts.play) { opts.play.call(that); }
		} else if (playing) {
			if (opts.pause) { opts.pause.call(that) };
		}
		// draw();
	}

	interact("canvas")
		.on("down", function (evt) {
			var bcr = evt.target.getBoundingClientRect(),
				lx = evt.clientX - bcr.left,
				ly = evt.clientY - bcr.top,
				a = Math.atan2((ly - cy), (lx - cx)),
				r = Math.sqrt(((lx-cx)*(lx-cx)) + ((ly-cy)*(ly-cy)));

			if (r < bRadius) {
				play();
			} else if ((Math.abs(a-curtime.mr) < TEN_DEGREES) && r < mRadius) {
				// console.log("dragging minutes");
				dragging = DRAGGING_MINUTES;
				draggingStartAngle = a;
			} else if ((Math.abs(a-curtime.sr) < TEN_DEGREES) && r < sRadius) {
				// console.log("dragging seconds");
				dragging = DRAGGING_SECONDS;
				draggingStartAngle = a;
			}
			if (dragging) {
				if (opts.dragstart) {
					opts.dragstart.call(that, {dragging: dragging});
				}
			}
		})
		.on("move", function (evt) {
			var dt;
			if (dragging) {
				var bcr = evt.target.getBoundingClientRect(),
					lx = evt.clientX - bcr.left,
					ly = evt.clientY - bcr.top,
					a = Math.atan2((ly - cy), (lx - cx)),
					r = Math.sqrt(((lx-cx)*(lx-cx)) + ((ly-cy)*(ly-cy))),
					delta = normalize(a - draggingStartAngle);

				draggingStartAngle = a;
				if (dragging == DRAGGING_SECONDS) {
					// adjust time
					dt = (delta / (Math.PI*2)) * 60;
				} else if (dragging == DRAGGING_MINUTES) {
					dt = (delta / (Math.PI*2)) * 3600;
				}
				var st = Math.max(0, Math.min(curtime_seconds + dt, duration_seconds));
				if (opts.dragmove) {
					opts.dragmove.call(this, st);
				}
				set_time(st);

			}
		})
		.on("up", function (evt) {
			if (dragging) {
				dragging = 0;
				if (opts.dragend) {
					opts.dragend.call(this);
				}
			}
		});
		//.draggable({ restrict: ... })
		//.on("dragstart", function (e) {
		//	console.log("dragstart");
		//});

	return that;
}
