function pdfwidget (elt, opts) {
	var that = {elt: elt},
		contents = document.createElement("div"),
		iframe = document.createElement("iframe"),
		dragshield = document.createElement("div"),
		viewer,
		numpages,
		curpage;

	elt.appendChild(contents);
	contents.appendChild(iframe);
	contents.appendChild(dragshield);

	contents.style.position = "absolute";
	contents.style.left = "10px";
	contents.style.top = "10px";
	contents.style.bottom = "10px";
	contents.style.right = "10px";

	VIEWER_URL = "/lib/pdf.js/build/generic/web/viewer.html";
	iframe.style.position = "absolute";
	iframe.style.left = "0";
	iframe.style.top = "0";
	iframe.style.width = "100%";
	iframe.style.height = "100%";
	iframe.style.border = "none";

	dragshield.style.position = "absolute";
	dragshield.style.left = "0";
	dragshield.style.top = "0";
	dragshield.style.width = "100%";
	dragshield.style.height = "100%";
	dragshield.style.zIndex = "1";
	dragshield.style.display = "none";

	iframe.addEventListener("load", function () {
		get_viewer();
	});

	var get_viewer = function () {
		if (iframe.contentWindow && iframe.contentWindow.PDFView !== undefined) {
			v = iframe.contentWindow.PDFView;
			viewer = v;
			// debugging
			// console.log("got pdf viewer", v);
			// window.pdf = v;
			init();
		} else {
			window.setTimeout(get_viewer, 250);
		}
	};

	function init () {
		// console.log("pdfwidget.init");
		if (opts.ready) {
			opts.ready.call(widget);
		}
		window.setInterval(function () {
			if (viewer.pagesCount !== numpages) {
				numpages = viewer.pagesCount;
				that.pages = numpages;
				if (opts.pageschange) {
					opts.pageschange.call(that, numpages);
				}
			}
			if (viewer.page !== curpage) {
				curpage = viewer.page;
				that.page = curpage;
				if (opts.pagechange) {
					opts.pagechange.call(that, viewer.page);
				}
			}
		}, 1000);		
	}

	interact(elt)
		.resizable({ edges : { left: true, top: true, right: false, bottom: false } })
		.on("resizestart", function (evt) {
			dragshield.style.display = "block";		
		})
		.on("resizeend", function (evt) {
			dragshield.style.display = "none";
		})
		.on("resizemove", function (evt) {
			elt.style.width = evt.rect.width + "px";
			elt.style.height = evt.rect.height + "px";
		});

	that.page = function (n) {
		if (n === undefined) {
			return v.page;
		} else {
			v.page = n;
		}
	}

	that.href = function (href) {
		iframe.src = VIEWER_URL + "?file="+href;
	}

	if (opts.href) {
		iframe.src = VIEWER_URL + "?file="+opts.href;
	} else {
		iframe.src = VIEWER_URL + "?file=";
	}

	return that;
}
