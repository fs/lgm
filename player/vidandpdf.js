var text = document.querySelector(".text"),
	monitor = document.querySelector(".monitor"),
	textarea = document.querySelector("textarea"),
	video = document.querySelector("video"),
	iframe = document.querySelector("#iframe"),
	swap = document.querySelector("#swap"),
	next = document.querySelector("#next"),
	record = function (time, page) {
		textarea.value += video.src + "#t=" + timecode(time) + "\n" + "page=" + page + "\n\n";
	};

video.style.left = "0";
video.style.top = "0";
video.style.width = "100%";
video.style.height = "100%";

var pdf_in_monitor = true,
	monitor_elt = iframe,
	monitor_compacted = false,
	monitor_height;

interact(".monitor .liner")
	.resizable({ edges : { left: true, top: true, right: false, bottom: false } })
	.on("resizestart", function (evt) {
		iframe.querySelector(".dragshield").style.display = "block";		
	})
	.on("resizeend", function (evt) {
		iframe.querySelector(".dragshield").style.display = "none";
	})
	.on("resizemove", function (evt) {
		// console.log("axes", evt.axes);
		console.log(evt.target.parentNode);
		// var x = evt.pageX - evt.x0, y = evt.pageY - evt.y0;
		// console.log("resizemove", evt.rect, evt.deltaRect);
		evt.target.parentNode.style.left = evt.rect.left + "px";
		// evt.target.parentNode.style.top = evt.rect.top + "px";
		var dx = evt.rect.width - parseInt(evt.target.parentNode.style.width);
		evt.target.parentNode.style.width = evt.rect.width + "px";
		evt.target.parentNode.style.height = evt.rect.height + "px";

		var mrect = evt.interactable.getRect(monitor);
		// console.log("mrect", mrect);

		monitor_elt.style.left = (evt.rect.left + 10) + "px";
		monitor_elt.style.top = (mrect.top + 10) + "px";
		monitor_elt.style.width = (evt.rect.width - 20) + "px";
		monitor_elt.style.height = (evt.rect.height - 10)+ "px";
		monitor_elt.style.zIndex = 10;

		// set text right edge
		// text.style.width = (parseInt(text.style.width) - dx) + "px";
	})
	.on("tap", function (evt) {
		if (!monitor_compacted) {
			// compact
			monitor_compacted = true;
			monitor_elt.style.display = "none";
			monitor_height = monitor.style.height;
			monitor.style.height = "10px";
		} else {
			// restore
			monitor_compacted = false;
			monitor.style.height = monitor_height;
			resize_monitor();
		}
	})

interact(".text .liner")
	.resizable({ edges : { left: false, top: true, right: true, bottom: false } })
	.on("resizestart", function (evt) {
		// document.getElementById("dragshield").style.display = "block";		
	})
	.on("resizeend", function (evt) {
		// document.getElementById("dragshield").style.display = "none";
	})
	.on("resizemove", function (evt) {
		// console.log("axes", evt.axes);
		// var x = evt.pageX - evt.x0, y = evt.pageY - evt.y0;
		// console.log("resizemove", evt.rect, evt.deltaRect);
		// evt.target.parentNode.style.left = evt.rect.left + "px";
		// evt.target.parentNode.style.top = evt.rect.top + "px";
		evt.target.parentNode.style.width = evt.rect.width + "px";
		evt.target.parentNode.style.height = evt.rect.height + "px";
	});

interact(".thumb")
	.draggable({
		restrict: "parent"
	})
	.on("dragstart", function (evt) {
	})
	.on("dragend", function (evt) {
	})
	.on("dragmove", function (evt) {
		var x = parseInt(evt.target.style.left || 0);
			// y = parseInt(evt.target.style.top || 0);
		// console.log(x, y, evt.dx, evt.dy);
		evt.target.style.left = (x + evt.dx) + "px";
		// evt.target.style.top = (y + evt.dy)  +"px";
	})

function resize_monitor () {
	var r = monitor.getBoundingClientRect();
	monitor_elt = pdf_in_monitor ? iframe: video;
	monitor_elt.style.left = (r.left + 10) + "px";
	monitor_elt.style.top = (r.top + 10) + "px";
	monitor_elt.style.width = (r.width - 20) + "px";
	monitor_elt.style.height = (r.height - 10) + "px";
	monitor_elt.style.zIndex = 10;
	monitor_elt.style.display = monitor_compacted ? "none" : "block";
	return r;
}
resize_monitor();

swap.addEventListener("click", function () {
	pdf_in_monitor = (!pdf_in_monitor);
	var r = resize_monitor();
	var other_elt = pdf_in_monitor ? video : iframe;
	other_elt.style.left = 0;
	other_elt.style.top = 0;
	other_elt.style.width = "100%";
	other_elt.style.height = "100%";
	other_elt.style.zIndex = 1;
	other_elt.style.display = "block";
}, false);

next.addEventListener("click", function () {
	var cp = pdfviewer.page();
	pdfviewer.page(cp+1);
}, false);

function pdfwidget (iframe, opts) {
	var cw = iframe.contentWindow,
		v,
		curpage,
		widget = {elt: iframe};

	function init () {
		console.log("pdfwidget.init");
		if (opts.ready) {
			opts.ready.call(widget);
		}
		window.setInterval(function () {
			if (v.page !== curpage) {
				curpage = v.page;
				if (opts.pagechange) {
					opts.pagechange.call(widget, v.page);
				}
			}
		}, 1000);		
	}

	(function () {
		var get_viewer = function () {
			if (cw.PDFView !== undefined) {
				v = cw.PDFView;
				widget.viewer = v;
				init();
			} else {
				window.setTimeout(get_viewer, 250);
			}
		};
		get_viewer();
	})();

	widget.page = function (n) {
		if (n === undefined) {
			return v.page;
		} else {
			v.page = n;
		}
	}

	return widget;
}

var timecode = function (t) {
	var h = Math.floor(t/3600),
		m, s, f;
	t -= (h*3600);
	m = Math.floor(t/60);
	t -= (m*60);
	s = Math.round(t);
	t -= s;
	f = t;
	return (h < 10 ? "0"+h : h)+":"+(m < 10 ? "0"+m : m)+":"+(s < 10 ? "0"+s : s);
}
var pdfviewer = pdfwidget(document.querySelector("iframe.pdf"), {
	pagechange: function (p) {
		console.log("page", p);
		record(video.currentTime, p);
	},
	ready: function () {
		console.log("pdf ready");
	}
});

// console.log(pdff, pdff.contentWindow.PDFView);

