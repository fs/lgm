/*

Element wrapper,
inspired by d3...
allows patching to shadow/augment native methods...

*/

function $el (selector, context) {
	var that = {
			selector: selector,
			context: context
		},
		attr_setters = {},
		elt;

	context = context || document.body;
	if (typeof(context) === "string") { context = document.querySelector(context); }
	if (context.hasOwnProperty("elt")) { context = context.elt; };

	elt = context.querySelector(selector);

	if (elt === null) {
		var selectorparts = selector.split("#"),
			tagname, id;
 			if (selectorparts.length > 1) {
			tagname = selectorparts[0];
			id = selectorparts[1]
		}
		elt = document.createElement(tagname);
		elt.setAttribute("id", id);
		context.appendChild(elt);
	}
	that.classed = function (classname, val) {
		if (val) {
			elt.classList.add(classname);
		} else {
			elt.classList.remove(classname);
		}
		return that;
	}
	that.style = function (arg) {
		if (arguments.length == 2) {
			var name = arguments[0],
				value= arguments[1];
			elt.style[name] = value;
		} else {
			for (var name in arg) {
				elt.style[name] = arg[name];
			}
		}
		return that;
	}
	that.prop = function (arg) {
		if (arguments.length == 1 && typeof(arg) == "string") {
			// get attribute by name
			return elt[arg];
		}
		else if (arguments.length == 2) {
			elt[arguments[0]] = arguments[1];
		} else {
			for (var name in arg) {
				elt[name] = arg[name];
			}
		}
		return that;
	}

	function setAttribute (name, value) {
		var cancel_default = false,
			setter, ret;
		if (attr_setters[name]) {
			var setter = attr_setters[name];
			ret = setter.call(that, value);
			if (ret === false) { cancel_default = true }
		}
		if (!cancel_default) {
			elt.setAttribute(name, value);
		}
	}

	that.patch = function (opts) {
		if (opts.attr) {
			for (var name in opts.attr) {
				attr_setters[name] = opts.attr[name];
			}
		}
		return that;
	}

	that.attr = function (arg) {
		if (arguments.length == 1 && typeof(arg) == "string") {
			return elt.getAttribute(arg);
		}
		else if (arguments.length == 2) {
			setAttribute([arguments[0]], arguments[1]);
		} else {
			for (var name in arg) {
				setAttribute(name, arg[name]);
			}
		}
		return that;
	}
	that.on = function (eventname, callback) {
		elt.addEventListener(eventname, function () {
			callback.call(that)
		}, false);
		return that;
	}
	that.call = function (name) {
		// console.log("call", elt, elt[name]);
		elt[name].call(elt);
	}
	that.text = function (text) {
		elt.innerHTML = text; // ???
	}
	that.html = function (text) {
		elt.innerHTML = text;
	}

	that.elt = elt;
	return that;
}
