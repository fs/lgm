function textwidget (elt, opts) {
	var that = {elt: elt},
		contents = document.createElement("div"),
		editor = document.createElement("textarea");

	elt.appendChild(contents);
	contents.appendChild(editor);

	contents.style.position = "absolute";
	contents.style.left = "0";
	contents.style.top = "0";
	contents.style.right = "0";
	contents.style.bottom = "0";

	editor.style.width = "100%";
	editor.style.height = "100%";
	editor.style.border = "none";
	editor.style.margin = "0";
	editor.style.padding = "0";

	interact(elt)
	.on("tap", function (e) {
		console.log("tap");
	});

	interact(contents)
	.resizable({ edges : { left: false, top: true, right: true, bottom: false } })
	.on("resizemove", function (evt) {
		elt.style.width = evt.rect.width + "px";
		elt.style.height = evt.rect.height + "px";
	})


	return that;
}
