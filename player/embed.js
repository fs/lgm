document.body.style.overflow = "hidden";
var html = document.querySelector("html");
html.style.overflow = "hidden"; // weird html elt has overflow-y: scroll rule
var links = document.querySelectorAll("a");

function pos (elt, o) {
	elt.style.position = (o.position !== undefined) ? o.position : "absolute";
	if (o.left !== undefined) { elt.style.left = ((typeof(o.left) == "number") ? (o.left+"px") : o.left); }
	if (o.top !== undefined) { elt.style.top = ((typeof(o.top) == "number") ? (o.top+"px") : o.top); }
	if (o.right !== undefined) { elt.style.right = ((typeof(o.right) == "number") ? (o.right+"px") : o.right); }
	if (o.bottom !== undefined) { elt.style.bottom = ((typeof(o.bottom) == "number") ? (o.bottom+"px") : o.bottom); }
	if (o.width !== undefined) { elt.style.width = ((typeof(o.width) == "number") ? (o.width+"px") : o.width); }
	if (o.height !== undefined) { elt.style.height = ((typeof(o.height) == "number") ? (o.height+"px") : o.height); }
	if (o.zIndex !== undefined) { elt.style.zIndex = o.zIndex; }
	return elt;
}

function has_class_recursive (n, classname) {
	while (n) {
		console.log("class", n);
		if (n.classList.contains(classname)) {
			return true;
		}
		n = n.parentNode;
	}
}

for (var i=0, l=links.length; i<l; i++) {
	links[i].addEventListener("click", function (e) {
		// console.log("click", e, this.href);
		if (/\.((mp4)|(ogv)|(webm))$/i.test(this.href)) {
			// console.log("video");
			display(this.href, "video");
			e.preventDefault();
		} else if (/\.((pdf))$/i.test(this.href)) {
			// console.log("pdf");
			display(this.href, "pdf");
			e.preventDefault();
		} else {
			// are we inside a program summary?
			if (has_class_recursive(e.target, "schedule_summary")) {
				display(this.href, "iframe");
				e.preventDefault();
			}
		}
	}, false);
}
var wrap = document.createElement("div"),
	controls_div = document.createElement("div");

wrap.classList.add("cc_wrapper");
document.body.appendChild(wrap);
var page = document.querySelector("#page");
wrap.appendChild(page);
page.style.overflow = "hidden";
wrap.style.position = "absolute";
wrap.style.zIndex = "1";
wrap.style.left = "0px";
wrap.style.top = "0px";
wrap.style.width = "100%";
wrap.style.height = "100%";
wrap.style.overflow = "auto";

document.body.appendChild(controls_div);
controls_div.classList.add("cc_controls_div");
controls_div.style.position = "absolute";
controls_div.style.left = "50%";
controls_div.style.bottom = "0";
controls_div.style.zIndex = "20";

var controls = document.createElement("div");
controls.classList.add("cc_controls");
controls.style.width = "30px";
controls.style.height = "30px";
controls.style.background = "black";
controls.style.position = "absolute";
controls.style.left = "-15px";
controls.style.bottom = "0";
controls_div.appendChild(controls);

controls.addEventListener("click", function () {
	if (showing_overlay) {
		hide_overlay();
	} else {
		show_overlay();
	}
}, false);

var showing_overlay = false, overlay_div;
function show_overlay() {
	overlay_div = document.createElement("div");
	pos(overlay_div, {
		left: 0,
		top: 0,
		width: "100%",
		height: "100%",
		zIndex: 10
	});
	overlay_div.classList.add("cc_overlay");
	document.body.appendChild(overlay_div);
	showing_overlay = true;
	var elts = [];
	for (var name in elts_by_target) { elts.push(elts_by_target[name]); }
	elts.forEach(function (elt) {
		// var elt = elts_by_target[name];
		var e = document.createElement("div");
		pos(e, {
			left: elt.style.left,
			top: elt.style.top,
			width: elt.style.width,
			height: elt.style.height
		})

		var url = document.createElement("span");
		e.style.textAlign = "center";
		url.style.color = "#FFF";
		url.style.background = "#444";
		url.style.padding = "5px";
		url.style.fontSize = "24px";
		url.style.fontFamily = "monospace";
		url.innerHTML = elt.src;
		e.appendChild(url);

		var closeButton = document.createElement("button");
		closeButton.innerHTML = "close";
		e.appendChild(closeButton);
		closeButton.addEventListener("click", function () {
			layout.remove_elt(elt);
			remove(elt);
			hide_overlay();
		})

		overlay_div.appendChild(e);

	});
}
function hide_overlay () {
	showing_overlay = false;
	// console.log("hide");
	document.body.removeChild(overlay_div);
	overlay_div = null;
}

// controls: close button + urls
// controls: rotate, shuffle

var elts_by_target = {};

function display (url, target) {
	var elt = elts_by_target[target];
	if (elt == undefined) {
		elt = document.createElement("iframe");
		document.body.appendChild(elt);
		elt.style.position = "absolute";
		layout.add_elt(elt);
		elts_by_target[target] = elt;
	}
	elt.src = url;
	layout.refresh();
};

function remove (elt) {
	for (var name in elts_by_target) {
		if (elts_by_target[name] === elt) {
			document.body.removeChild(elt);
			elt.src = "";
			delete elts_by_target[name];
		}
	}
}

/************ LAYOUT *************/
var layout = (function () {
	var that = {},
		cells = [];

	that.add_elt = function (elt) {
		cells.push({elt: elt});
	}

	that.remove_elt = function (elt) {
		for (var c=0, l=cells.length; c<l; c++) {
			if (cells[c].elt === elt) {
				cells.splice(c, 1);
				layout();
				return;
			}
		}
	}
	function layout () {
		var wh = window.innerHeight,
			ww = window.innerWidth,
			hcells = 2,
			vcells = 2,
			cell_width = ww / hcells,
			cell_height = wh / vcells;
		// console.log("window size:", ww, wh);
		var x, y, w = 1, h = 1, z = 0;
		for (var c=0, l=cells.length; c<l; c++) {
			var elt = cells[c].elt;
			if (cells.length == 1) {
				x = 0; y = 0; w = 2; h = 2;
			} else if (cells.length == 2) {
				if (c == 0) {
					x = 0; y = 0; w = 1; h = 2;
				} else {
					x = 1; y = 0; w = 1; h = 2;
				}
			} else if (cells.length == 3) {
				if (c == 0) {
					x = 0; y = 0; w = 1; h = 2;
				} else if (c == 1) {
					x = 1; y = 0; w = 1; h = 1;
				} else if (c == 2) {
					x = 1; y = 1; w = 1; h = 1;
				}			
			} else if (cells.length == 4) {
				if (c == 0) {
					x = 0; y = 0; w = 1; h = 1;
				} else if (c == 3) {
					x = 0; y = 1; w = 1; h = 1;
				} else if (c == 1) {
					x = 1; y = 0; w = 1; h = 1;
				} else if (c == 2) {
					x = 1; y = 1; w = 1; h = 1;
				}			
			}
			elt.style.left = (x * cell_width) + "px";
			elt.style.top = (y * cell_height) + "px";
			elt.style.width = (w * cell_width) + "px";
			elt.style.height = (h * cell_height) + "px";
			elt.style.zIndex = z;
		}
	}

	that.refresh = layout;
	return that;
})();
/******************************/


layout.add_elt(wrap);

window.addEventListener("resize", function () { layout.refresh(); })
