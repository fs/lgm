
function html5video (elt) {
    var that = {elt: elt},
        sessionsByHref = {},
    	media,
        current_href_nofrag,
        eventHandlers = {};

    function aa_parse_fragment (fragment) {
        var parts = fragment.split('&'),
            part, m;
        for (var i=0, l=parts.length; i<l; i++) {
            part = parts[i];
            m = part.match(/^t=(.+?)(?:,(.+))?$/);
            if (m !== null) {
                this.timeStart = m[1];
                if (m[2]) {
                    this.timeEend = m[2];
                }
            }
            m = part.match(/^line=(.+?)(?:,(.+))?$/);
            if (m !== null) {
                this.lineStart = parseInt(m[1]);
                if (m[2]) {
                    this.lineEnd = parseInt(m[2]);
                }
            }
        }
    }

    function aa_href (href) {
        var mf = window.MediaFragments.parse(href),
            that = {
                href: href
            },
            hashpos = href.indexOf('#'),
            base = href,
            fragment = null;
        if (hashpos >= 0) {
            base = href.substr(0, hashpos);
            fragment = href.substr(hashpos+1);
            aa_parse_fragment.call(that, fragment);
        }
        that['base' ] = base;
        that['nofrag' ] = base;
        that['basehref' ] = base;
        that['fragment'] = fragment;
        var lsp = base.lastIndexOf("/");
        that['basename'] = (lsp !== -1) ? base.substr(lsp+1) : base; 

        if (mf.hash && mf.hash.t && mf.hash.t.length >= 1) {
            that['start'] = mf.hash.t[0].startNormalized;
            that['end'] = mf.hash.t[0].endNormalized;
        }

        return that;
    }

	function hmsf (t) {
		var h, m, s, raw = t;
		h = Math.floor(t/3600); t -= (h*3600);
		m = Math.floor(t/60); 	t -= (m*60);
		s = Math.floor(t); 		t -= s;
		return { h: h, m: m, s: s, f: t, raw: raw };
	}

	timecode_styles = {
	    'srt': {
	        requireHours: true,
	        delimiter: ','
	    },
	    'html5': {
	        requireHours: false,
	        delimiter: '.'
	    }
	}

	function timecode (ss, style) {
	    style = timecode_styles[style || 'html5'];
	    var t = hmsf(ss), ret;

	    fract = style.delimiter + (""+t.f).substr(2, 3);
	    while (fract.length < 4) { fract += "0"; } // PAD TO ALWAYS BE THREE DIGITS LONG (eg .040)
	    ret = (t.h || style.requireHours ? ((((t.h<10)?"0":"")+t.h)+":") : '')
	    ret += (((t.m<10)?"0":"")+t.m)+":"+(((t.s<10)?"0":"")+t.s)+fract;
	    return ret;
	}

	function parse_timecode (tc) {
	    var tcpat = /^(?:(\d\d):)?(\d\d):(\d\d(?:[\.,](?:\d{1,3}))?)$/,
	        groups = tcpat.exec(tc);
	    if (groups != null) {
	        var h = groups[1] !== undefined ? parseInt(groups[1]) : 0,
	            m = parseInt(groups[2]),
	            s = parseFloat(groups[3].replace(/,/, "."));
	        return (h * 3600) + (m * 60) + s;
	    }
	}

	function timeRangeHref (href, start, end) {
	    var ret = href;
	    if (start) { ret += "#t=" + timecode(start, 'html5'); }
	    if (end) { ret += "," + timecode(end, 'html5'); }
	    return ret;
	}

    elt.classList.add('html5video');

    function hide (e) { e.style.display = "none"; }
    function show (e) { e.style.display= "block"; }
    function visible (e) { e.style == "block" }

    function set_contents (m) {
        // console.log("hello set_contents", $(editor.elt).contents().get(0), i);
        if (visible(m)) {
            // console.log("set_contents... doing nothing");
            return;
        }
        if (media) { hide(media) };
        media = m;
        show(m);
    }

    that.href = function (href, done) {
        if (arguments.length == 0) { return timeRangeHref(current_href_nofrag, media.currentTime); }
        var href = aa_href(href),
            session = sessionsByHref[href.nofrag];

        current_href_nofrag = href.nofrag;

        if (session == "loading") { return false; }

        if (session != undefined) {
            // console.log("reusing iframe");
            if (done) {
                window.setTimeout(function () {
                    done.call(session);
                }, 0);
            }
            if (href.start) {
                // console.log("jumping to time", href.start);
                session.media.currentTime = href.start;
                session.media.play();                    
            }
            set_contents(session.media);
            return true;
        }

        // console.log("CREATE IFRAME!!!!");
        sessionsByHref[href] = "loading";
        session = { href: href.nofrag, player: that }
        session.media = document.createElement("video");
        session.media.style.width = "100%";
        session.media.style.height = "100%";
        for (var eventname in eventHandlers) {
            session.media.addEventListener(eventname, eventHandlers[eventname]);
        }
        session.media.setAttribute("controls", "");
        elt.appendChild(session.media);
        hide(session.media);
        /*
        session.media.addEventListener("timeupdate", function (e) {
            var media = this;
            // console.log("timeupdate", e, this);
            $(that.elt).trigger("fragmentupdate", { player: that, originalEvent: e, originalTarget: e.target });                            
        }, false);
        */
        sessionsByHref[href.nofrag] = session;
        if (done) {
            window.setTimeout(function () {
                done.call(session);
            }, 0);
        }
        set_contents(session.media);
        session.media.src = href.href;
    }

    that.toggle = function () {
        if (media) { media.paused ? media.play() : media.pause(); }
        return that;
    }

    that.play = function () {
        if (media) { media.play(); }
        return that;
    }

    that.pause = function () {
        if (media) { media.pause(); }
        return that;
    }

    that.jumpBack = function () {
        if (media) { media.currentTime = media.currentTime - 5; }
        return that;
    }

    that.jumpForward = function () {
        if (media) { media.currentTime = media.currentTime + 5; }
        return that;
    }

    that.currentTime = function (t) {
        if (arguments.length == 0) {
            if (media) {
                return media.currentTime;
            }           
        } else {
            if (media) {
                media.currentTime = t;
            }           
            return that;
        }
    }

    that.duration = function () {
        if (media) { return media.duration; }
    }

    that.addEventListener = function (event, callback) {
        eventHandlers[event] = callback;

    }

    return that;
};
