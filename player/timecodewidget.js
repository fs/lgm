function timecodewidget (elt, opts) {
	var time = document.createElement("div"),
		that = {elt: elt},
		curtime;

	time.classList.add("timecodewidget");
	elt.appendChild(time);

	function hmsf (t) {
		var h, m, s, raw = t;
		h = Math.floor(t/3600); t -= (h*3600);
		m = Math.floor(t/60); 	t -= (m*60);
		s = Math.floor(t); 		t -= s;
		return { h: h, m: m, s: s, f: t, raw: raw };
	}

	that.currentTime = function (t) {
		var c;
		if (t !== undefined) {
			// setter
			t = hmsf(t);
			// console.log(t);
			c = ((t.h<10)?"0"+t.h:t.h)+":"+((t.m<10)?"0"+t.m:t.m)+":"+((t.s<10)?"0"+t.s:t.s);
			time.innerHTML = c;
			curtime = t;
		} else {
			//getter
			return curtime !== undefined ? curtime.raw : undefined;
		}
	}
	return that;
}