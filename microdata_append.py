from __future__ import print_function
import json, sys
from argparse import ArgumentParser
from unidecode import unidecode

p = ArgumentParser("")
p.add_argument("--output", default=None, help="output")
p.add_argument("input", nargs="*", default=[], help="input")
args = p.parse_args()

data = {}
data['terms'] = terms = {}

for i in args.input:
	print ("Reading {0}".format(i), file=sys.stderr)
	with open(i) as f:
		d = json.load(f)
		for item in d['items']:
			item['doc'] = i
			for role, values in item['properties'].items():
				for v in values:
					vkey = unidecode(v.strip().lower())
					if vkey not in terms:
						terms[vkey] = {'term': vkey, 'forms': [], 'docs': [], 'roles': []}
					terms[vkey]['docs'].append(item)
					if role not in terms[vkey]['roles']:
						terms[vkey]['roles'].append(role)
					if v not in terms[vkey]['forms']:
						terms[vkey]['forms'].append(v)

# flatten terms to sorted list
data['terms'] = [terms[t] for t in sorted(terms.keys())]

if args.output:
	out = open(args.output, "w")
else:
	out = sys.stdout

json.dump(data, out, indent=2)
